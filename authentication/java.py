from rest_framework.authentication import BaseAuthentication
from .models import CustomUser
import requests

# Authenticate with the existing java backend and return a user object


class JavaBackendAuth(BaseAuthentication):
    www_authenticate_realm = 'api'

    def authenticate(self, request):
        # return tuple of (user, auth) or None if we can't authenticate
        # If we can authenticate but it fail raise AuthenticationFailed which prevents future checks
        headers = {'Authorization': request.META.get(
            'HTTP_AUTHORIZATION', None)}
        res = requests.get(
            'https://api.updraft.co.uk/users/me', headers=headers)
        if res.status_code == 200:
            if not res.json()['success']:
                return None
            java_id = res.json().get('object', {})['id']
            user, created = CustomUser.objects.get_or_create(
                java_id=java_id, username=f'java-{java_id}')
            return (user, None)
        return None
