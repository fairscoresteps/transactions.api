from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class CustomUser(AbstractUser):
    java_id = models.PositiveIntegerField(
        unique=True, blank=True, null=True, default=None)

    def __str__(self):
        if self.java_id:
            return f'Java:{self.java_id}'
        else:
            return super().__str__()