from django.test import TestCase, RequestFactory
import responses
from .java import JavaBackendAuth
from .models import CustomUser


class TestBasicAuthentication(TestCase):

    def setUp(self):
        super().setUp()
        self.factory = RequestFactory()
        self.good_user = {
            "object": {
                "id": 310,
                "creationDate": "2019-04-03 16:50:45",
                "modificationDate": "2019-04-05 13:58:00",
                "email": "matt@emillar.com",
                "title": "Mr",
                "firstname": "M",
                "lastname": "Millar",
                "fullName": "M C Millar",
                "bankEmail": "matt@emillar.com",
                "searchSignature": "MCm",
                "searchDate": "05/04/2019",
                "enabled": True,
                "blocked": False,
                "termsAccepted": False,
                "phone": "447900496487",
                "pictureURL": "https://updraft-agreements.s3.eu-west-2.amazonaws.com/?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20190418T115145Z&X-Amz-SignedHeaders=host&X-Amz-Expires=900&X-Amz-Credential=AKIAINKWVPWKIQTM2XQA%2F20190418%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Signature=de5991dcce6f8655046a748783100ff252627ff89ff170d0d49e1c974c8962c8",
                "phoneVerified": True,
                "dateOfBirthday": "1973-02-13",
                "employmentStatus": "Employed",
                "occupation": "cto",
                "annualIncome": 50000.0,
                "waiting": False,
                "elegibility": True,
                "softSearch": True,
                "authorities": [{
                    "id": 310,
                    "user": 310,
                    "role": {
                                "id": 2,
                                "name": "ROLE_CUSTOMER"
                                }
                }],
                "address": {
                    "id": 310,
                    "creationDate": "2019-04-03 16:50:45",
                    "modificationDate": "2019-04-03 16:55:30",
                    "addr1": "GREYSMEAD",
                    "addr2": "THAME PARK ROAD",
                    "addr3": "",
                    "city": "THAME",
                    "state": "OXFORDSHIRE",
                    "zip": "OX9 3PL",
                    "residentialStatus": "Homeowner",
                    "yearsAtCurrentAddress": 11,
                    "fullAddressInline": "GREYSMEAD, THAME PARK ROAD"
                },
                "cashAdvance": False,
                "approvedForLoans": True,
                "yearsAtCurrentAddress": 11,
                "residentialStatus": "Homeowner",
                "userValidForCredit": True,
                "age": 46,
                "errorsForCredit": []
            },
            "success": True
        }

    @responses.activate
    def test_authentication_success(self):
        auth = JavaBackendAuth()
        responses.add(responses.GET,
                      'https://api.updraft.co.uk/users/me',
                      json=self.good_user)
        auth.authenticate(self.factory.get('/hello/'))
        assert CustomUser.objects.all().count() == 1
        assert CustomUser.objects.get(
            java_id=self.good_user['object']['id']).java_id == self.good_user['object']['id']

    @responses.activate
    def test_authentication_failure_401(self):
        auth = JavaBackendAuth()
        responses.add(responses.GET,
                      'https://api.updraft.co.uk/users/me',
                      status=401)
        result = auth.authenticate(self.factory.get('/hello/'))
        assert result is None
        assert CustomUser.objects.all().count() == 0

    @responses.activate
    def test_authentication_failure_bad_user(self):
        auth = JavaBackendAuth()
        responses.add(responses.GET,
                      'https://api.updraft.co.uk/users/me',
                      json={'success': False})
        result = auth.authenticate(self.factory.get('/hello/'))
        assert result is None
        assert CustomUser.objects.all().count() == 0
