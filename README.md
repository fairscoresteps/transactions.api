# Transactions microservice


[![Build Status](https://codebuild.eu-west-2.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoiNzlOMHloUFRQOWVWQ0QzZWIvbTFQeHdyMkdxdWdDeGordGhhRnVhQTRBV1RlOGRRQmVualZMbm1uZVhCRFVFeXo1NHB5S3NaTEYxOU1kbGZMTm5qc1VNPSIsIml2UGFyYW1ldGVyU3BlYyI6IlVGbjd4aXg0T1pGZHRqWXUiLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)](https://eu-west-2.console.aws.amazon.com/codesuite/codebuild/projects/transactions_api)

Deployment uses IAM policy 

arn:aws:iam::104020910987:policy/ZappaTransactionDeployPolicy

On Codebuild

https://eu-west-2.console.aws.amazon.com/codesuite/codebuild/projects/transactions_api/history