ALLOWED_HOSTS = ['.execute-api.eu-west-2.amazonaws.com',
                 '.api.updraft.co.uk',
                 ]

# this is only needed if we are deploying to a custom domain on .api.updraft.co.ul
STATIC_URL = '/static/'

# True Layer Settings
# For truelayer account with login matt.millar@updraft.com
TRUELAYER_CLIENT_ID = "updraftmaster-52db7d"
TRUELAYER_ENABLE_MOCK = True
DOMAIN = "transaction-master.api.updraft.co.uk"