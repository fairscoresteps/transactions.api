"""service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from truelayer import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('connect/', views.true_layer_connect, name='true_layer_connect'),
    path('authentication/complete/', views.TrueLayerRedirectUriAPI.as_view(), name='true_layer_redirect_uri'),
    path('webhook_uri/', views.TrueLayerWebhookUriAPI.as_view(), name='true_layer_webhook_uri'),

    path('profile/accounts/', views.BankAccountsListView.as_view(), name='user_accounts'),
    path('profile/accounts/refresh/', views.BankAccountsListRefreshView.as_view(), name='force_refresh_user_accounts'),
    path('profile/accounts/<int:account_pk>/transactions/',
         views.TransactionsListView.as_view(), name='account_transactions'),
    path('profile/accounts/<int:account_pk>/transactions/refresh/',
         views.TransactionsListRefreshView.as_view(), name='force_refresh_transactions'),
    path('profile/accounts/<int:pk>/update/', views.BankAccountUpdateView.as_view(), name='update_account'),

    path('profile/cards/', views.CreditCardsListView.as_view(), name='user_cards'),
    path('profile/cards/refresh/', views.CreditCardsListRefreshView.as_view(), name='force_refresh_user_cards'),
    path('profile/cards/<int:pk>/update/', views.CreditCardUpdateView.as_view(), name='update_card'),
]
