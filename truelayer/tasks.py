import logging
import pytz
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.db import transaction
from rest_framework import status
from zappa.asynchronous import task, run
from authentication.models import CustomUser

from truelayer.models import AsyncRequest, BankAccount, AccountBalance, CreditCard
from truelayer.serializers import (CreateBankAccountSerializer,
                                   CreateTransactionSerializer,
                                   CardBalanceSerializer, BalanceSerializer,
                                   CreateCreditCardSerializer)

from truelayer.requests import send_request_to_true_layer, send_async_request_to_true_layer

logger = logging.getLogger(__name__)


def request_accounts(token, url=None):
    '''Create and update list of accounts'''
    url = url or settings.TRUELAYER_ACCOUNTS_URL
    response = send_request_to_true_layer(url=url, token_id=token.id)
    if response.status_code != status.HTTP_200_OK or 'results' not in response.json().keys():
        logger.error(f"Unexpected response for Accounts (Token {token.id}): {response.status_code} {response.json()}")
        return
    serializer = CreateBankAccountSerializer(data=response.json()['results'],
                                             context={'bank_profile': token.bankprofile},
                                             many=True)
    if not serializer.is_valid():
        logger.error(f"Failed to update Accounts for token {token.id}."
                     f"Invalid data in response: {serializer.errors}")
        return

    with transaction.atomic():
        serializer.save()
    logger.info('Finished saving accounts')


def request_credit_cards(token, url=None):
    '''Create and update list of credit cards'''
    url = url or settings.TRUELAYER_CARDS_URL
    response = send_request_to_true_layer(url=url, token_id=token.id)
    if response.status_code != status.HTTP_200_OK or 'results' not in response.json().keys():
        logger.error(f"Unexpected response for Credit Cards (Token {token.id}): {response.status_code} {response.json()}")
        return
    serializer = CreateCreditCardSerializer(data=response.json()['results'],
                                            context={'bank_profile': token.bankprofile},
                                            many=True)
    if not serializer.is_valid():
        logger.error(f"Failed to update Accounts for token {token.id}."
                     f"Invalid data in response: {serializer.errors}")
        return

    with transaction.atomic():
        serializer.save()
    logger.info('All cards saved')


def request_async_transactions(account_id, token_id, period=None, from_time=None, to_time=None):
    period_to_date_mapping = {
        'week': datetime.now(tz=pytz.utc) - timedelta(days=8),
        'month': datetime.now(tz=pytz.utc).replace(day=1) - relativedelta(months=1),
        'year': datetime.now(tz=pytz.utc) - relativedelta(days=364)  # True Layer limit is "< 365 days"
    }
    url_params = {
        'from': (from_time or period_to_date_mapping.get(period, 'week')).isoformat(),
        'to': (to_time or datetime.now(tz=pytz.utc)).isoformat()
    }
    print(f'Sending request for period {period}')
    send_async_request_to_true_layer(url=settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=account_id),
                                     token_id=token_id, request_type=AsyncRequest.TYPE_TRANSACTIONS,
                                     url_params=url_params, extra_data={'account_id': account_id})


def request_transactions_for_account(account_id, token, url=None, params=None):
    '''Create and update list of transactions'''
    account = BankAccount.objects.get(account_id=account_id, bank_profile=token.bankprofile)
    if not url:
        url = settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=account_id)

    response = send_request_to_true_layer(url=url, token_id=token.id, params=params)
    if response.status_code != status.HTTP_200_OK or 'results' not in response.json():
        logger.error(f"Unexpected response for Transactions (Account {account.id}): {response.json()}")
        return
    serializer = CreateTransactionSerializer(data=response.json()['results'],
                                             context={'bank_account_id': account.id},
                                             many=True)
    if not serializer.is_valid():
        logger.error(f"Failed to update Transactions for account {account.id}."
                     f"Invalid data in response: {serializer.errors}")
        return
    with transaction.atomic():
        serializer.save()
    logger.info(f'All transactions saved for account_id {account_id}')


def request_balance(token, bank_account):
    response = send_request_to_true_layer(
        url=settings.TRUELAYER_BALANCE_URL.format(account_id=bank_account.account_id),
        token_id=token.id)
    if response.status_code != status.HTTP_200_OK or len(response.json().get('results', [])) == 0:
        logger.error(f"Unexpected response for Balance (Account {bank_account.id}) (Token {token.id}): {response.json()}")
        return
    balance_data = response.json()['results'][0].copy()
    balance_data['bank_account'] = bank_account.id
    serializer = BalanceSerializer(data=balance_data)
    if not serializer.is_valid():
        logger.error(f"Failed to update Balance for account {bank_account.id}."
                     f"Invalid data in response: {serializer.errors}")
        return
    balance = serializer.save()
    logger.info(f"Balance for account {bank_account.id} saved")
    return balance


def request_balance_for_card(token, credit_card):
    response = send_request_to_true_layer(
        url=settings.TRUELAYER_CARD_BALANCE_URL.format(account_id=credit_card.account_id),
        token_id=token.id)
    if response.status_code != status.HTTP_200_OK or len(response.json().get('results', [])) == 0:
        logger.error(f"Unexpected response for Balance (Card {credit_card.id}) (Token {token.id}): {response.json()}")
        return
    balance_data = response.json()['results'][0].copy()
    balance_data['credit_card'] = credit_card.id
    serializer = CardBalanceSerializer(data=balance_data)
    if not serializer.is_valid():
        logger.error(f"Failed to update Balance for credit card {credit_card.id}."
                     f"Invalid data in response: {serializer.errors}")
        return
    balance = serializer.save()
    logger.info(f"Balance for card {credit_card.id} saved")
    return balance


REQUEST_FUNCTIONS = {
    AsyncRequest.TYPE_ACCOUNTS: request_accounts,
    AsyncRequest.TYPE_TRANSACTIONS: request_transactions_for_account,  # TO DO: async request for account transactoins
}


@task
def process_request_to_true_layer(async_request_id):
    '''True Layer has processed a request of ours and we can now get the results'''
    async_request = AsyncRequest.objects.get(id=async_request_id)
    # map async request types to functions that will deal with them
    request_function_name = REQUEST_FUNCTIONS[async_request.request_type]
    request_function_name(token=async_request.token, url=async_request.results_uri, **async_request.extra_data)


# update at 2 am
# considering user.last_login as latest activity for now
def periodic_update_accounts_and_transactions():
    users_active_this_week = CustomUser.objects.filter(last_login__gte=datetime.now(tz=pytz.utc) - timedelta(days=7))
    users_inactive_for_more_than_week = CustomUser.objects.filter(
        last_login__gte=datetime.now(tz=pytz.utc) - timedelta(days=90),
        last_login__lte=datetime.now(tz=pytz.utc) - timedelta(days=7))

    for user in users_active_this_week:
        run(update_user_accounts_and_transactions, user_id=user.id)

    for user in users_inactive_for_more_than_week:
        inactive_period = datetime.now(tz=pytz.utc) - user.last_login
        if inactive_period.days % 7 == 0:
            # today is exactly n weeks since last login, so make update
            run(update_user_accounts_and_transactions, user_id=user.id)


def update_user_accounts_and_transactions(user_id):
    try:
        user = CustomUser.objects.get(id=user_id)
        user_profiles = user.bank_profiles.all()
        for profile in user_profiles:
            request_accounts(token=profile.token)
        accounts = BankAccount.objects.filter(bank_profile__in=user_profiles)
        for account in accounts:
            request_balance(token=account.bank_profile.token, bank_account=account)
            request_async_transactions(account.id, account.bank_profile.token_id)
    except Exception:
        logger.exception(f"Failed to update data for user {user_id}")
    else:
        logger.info(f"Finished updating accounts, balance and transactions for user {user_id}.")


def update_user_accounts_and_balances(user_id):
    try:
        user = CustomUser.objects.get(id=user_id)
        user_profiles = user.bank_profiles.all()
        for profile in user_profiles:
            request_accounts(token=profile.token)
        accounts = BankAccount.objects.filter(bank_profile__in=user_profiles)
        for account in accounts:
            request_balance(token=account.bank_profile.token, bank_account=account)
    except Exception:
        logger.exception(f"Failed to update accounts and balances for user {user.id}")
    else:
        logger.info(f"Finished updating accounts and balances for user {user.id}.")


def update_user_cards_and_balances(user_id):
    try:
        user = CustomUser.objects.get(id=user_id)
        user_profiles = user.bank_profiles.all()
        for profile in user_profiles:
            request_credit_cards(token=profile.token)
        cards = CreditCard.objects.filter(bank_profile__in=user_profiles)
        for card in cards:
            request_balance_for_card(token=card.bank_profile.token, credit_card=card)
    except Exception:
        logger.exception(f"Failed to update cards and balances for user {user.id}")
    else:
        logger.info(f"Finished updating cards and balances for user {user.id}.")
