

class RequestFailedException(Exception):
    pass


class ResponseValidationError(Exception):
    def __init__(self, errors, response, status, msg=None):
        msg = msg or f"Failed to validate received data."
        result_msg = (f"{msg} "
                      f"URL: {response.request.path_url} "
                      f"Response: {response.status_code} {response.json()} "
                      f"Errors: {errors}")
        super(ResponseValidationError, self).__init__(result_msg)
        self.errors = errors
        self.response = response
        self.status = status
