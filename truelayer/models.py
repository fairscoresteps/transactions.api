import pytz
from datetime import datetime, timedelta
from django.db import models
from django.conf import settings

# Aurora Serverless is mysql 5.6 so we can't use this
# from django_mysql.models import JSONField
from jsonfield import JSONField


class TimeStampedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Provider(TimeStampedModel):
    display_name = models.CharField(max_length=255)
    provider_id = models.CharField(max_length=255, unique=True)
    logo_uri = models.TextField()

    def __str__(self):
        return self.display_name


class Token(TimeStampedModel):
    '''Access_token details. Received from True Layer after user
    authenticated and gave us permissions to get info on their accounts.
    Used to request accounts & transactions data from True Layer.
    '''
    access_token = models.TextField()
    expires_in = models.IntegerField()
    token_type = models.CharField(max_length=255)
    refresh_token = models.CharField(max_length=255)

    raw_response = JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def expires_soon(self):
        '''True if already expired or if it expires in the next minute.
        False otherwise.
        '''
        expires_at = self.updated_at + timedelta(seconds=self.expires_in)
        if expires_at < datetime.now(tz=pytz.utc) + timedelta(minutes=1):
            return True
        return False


class BankProfile(TimeStampedModel):
    name = models.CharField(max_length=255, blank=True, null=True)
    provider = models.ForeignKey(Provider, on_delete=models.PROTECT)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name='bank_profiles')
    token = models.OneToOneField(Token, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.user} profile {self.id} [{self.name}]"


class Balance(TimeStampedModel):
    currency = models.CharField(max_length=3)
    available = models.DecimalField(max_digits=15, decimal_places=2)
    current = models.DecimalField(max_digits=15, decimal_places=2)
    update_timestamp = models.DateTimeField()

    class Meta:
        abstract = True


class AccountBalance(Balance):
    bank_account = models.ForeignKey(
        'BankAccount', on_delete=models.CASCADE, related_name='balances')
    overdraft = models.DecimalField(
        max_digits=15, decimal_places=2, null=True, blank=True)


class CardBalance(Balance):
    credit_card = models.ForeignKey('CreditCard', on_delete=models.CASCADE, related_name='balances')
    credit_limit = models.DecimalField(max_digits=15, decimal_places=2)
    last_statement_balance = models.DecimalField(
        max_digits=15, decimal_places=2, null=True, blank=True)
    last_statement_date = models.DateTimeField()
    payment_due = models.DecimalField(
        max_digits=15, decimal_places=2, null=True, blank=True)
    payment_due_date = models.DateTimeField(blank=True, null=True)


class BankAccount(TimeStampedModel):
    STATUS_VISIBLE = 'VISIBLE'
    STATUS_USER_HIDDEN = 'USER_HIDDEN'
    STATUS_CHOICES = (
        (STATUS_VISIBLE, 'Visble'),
        (STATUS_USER_HIDDEN, 'Hidden')
    )

    bank_profile = models.ForeignKey(
        BankProfile, on_delete=models.CASCADE, related_name='bank_accounts')
    update_timestamp = models.DateTimeField()
    account_id = models.CharField(max_length=255)
    account_type = models.CharField(max_length=255)
    display_name = models.CharField(max_length=255)
    currency = models.CharField(max_length=3)

    swift_bic = models.CharField(max_length=11)
    number = models.CharField(max_length=255)
    sort_code = models.CharField(max_length=255)

    # since we're not absolutely sure this provider will be the same as the token provider
    # we keep it for reference
    provider = models.ForeignKey(Provider, on_delete=models.PROTECT)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    status = models.CharField(
        max_length=20, choices=STATUS_CHOICES, default=STATUS_VISIBLE)

    class Meta:
        unique_together = ('account_id', 'bank_profile')

    @property
    def balance(self):
        try:
            return self.balances.latest('update_timestamp')
        except AccountBalance.DoesNotExist:
            return None


class CreditCard(TimeStampedModel):
    STATUS_VISIBLE = 'VISIBLE'
    STATUS_USER_HIDDEN = 'USER_HIDDEN'
    STATUS_CHOICES = (
        (STATUS_VISIBLE, 'Visble'),
        (STATUS_USER_HIDDEN, 'Hidden')
    )

    bank_profile = models.ForeignKey(
        BankProfile, on_delete=models.CASCADE, related_name='credit_cards')
    account_id = models.CharField(max_length=255)
    account_type = models.CharField(max_length=255)
    card_network = models.CharField(max_length=255)
    card_type = models.CharField(max_length=255)
    currency = models.CharField(max_length=3)
    display_name = models.CharField(max_length=255)
    partial_card_number = models.CharField(max_length=255)
    name_on_card = models.CharField(max_length=255, blank=True, null=True)
    valid_from = models.CharField(max_length=7, blank=True, null=True)
    valid_to = models.CharField(max_length=7, blank=True, null=True)
    update_timestamp = models.DateTimeField()
    provider = models.ForeignKey(Provider, on_delete=models.PROTECT)

    status = models.CharField(
        max_length=20, choices=STATUS_CHOICES, default=STATUS_VISIBLE)

    class Meta:
        unique_together = ('account_id', 'bank_profile')

    @property
    def balance(self):
        try:
            return self.balances.latest('update_timestamp')
        except CardBalance.DoesNotExist:
            return None


class Transaction(TimeStampedModel):
    bank_account = models.ForeignKey(
        BankAccount, on_delete=models.CASCADE, related_name='transactions')
    transaction_id = models.CharField(max_length=255)
    amount = models.DecimalField(max_digits=15, decimal_places=2)
    currency = models.CharField(max_length=3)
    timestamp = models.DateTimeField()
    transaction_type = models.CharField(max_length=255)
    transaction_category = models.CharField(max_length=255)
    transaction_classification = JSONField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    merchant_name = models.CharField(max_length=255, null=True, blank=True)
    meta = JSONField(null=True, blank=True)

    class Meta:
        unique_together = 'transaction_id', 'bank_account'


class AsyncRequest(TimeStampedModel):
    STATUS_QUEUED = 'Queued'
    STATUS_SUCCEEDED = 'Succeeded'
    STATUS_FAILED = 'Failed'

    STATUS_CHOICES = (
        (STATUS_QUEUED, 'Queued'),
        (STATUS_SUCCEEDED, 'Succeeded'),
        (STATUS_FAILED, 'Failed')
    )

    TYPE_ACCOUNTS = 'accounts'
    TYPE_TRANSACTIONS = 'transactions'

    REQUEST_TYPE_CHOICES = (
        (TYPE_ACCOUNTS, 'Accounts'),
        (TYPE_TRANSACTIONS, 'Transactions'),
    )
    # fields below are used to track what kind if data is requested
    request_type = models.CharField(
        max_length=20, choices=REQUEST_TYPE_CHOICES)
    extra_data = JSONField(null=True, blank=True)

    task_id = models.CharField(max_length=255, unique=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    results_uri = models.URLField()
    token = models.ForeignKey(Token, on_delete=models.CASCADE)

    # fields bellow are filled when TrueLayer hits the webhook_uri
    # to notify if results are ready or something went wrong
    request_uri = models.URLField(null=True, blank=True)
    credentials_id = models.CharField(max_length=255, null=True, blank=True)
    request_timestamp = models.DateTimeField(null=True, blank=True)
    error_description = models.TextField(null=True, blank=True)
    error = models.CharField(max_length=255, null=True, blank=True)
