from django.conf import settings
from django.urls import reverse


def get_redirect_uri():
    return "https://" + settings.DOMAIN + reverse('true_layer_redirect_uri')


def get_webhook_uri():
    return "https://" + settings.DOMAIN + reverse('true_layer_webhook_uri')
