from django.contrib import admin
from truelayer.models import (Token,
                              BankAccount, AccountBalance, CardBalance, Transaction, CreditCard,
                              Provider, BankProfile, AsyncRequest)


class TimeStampedAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', 'updated_at')


@admin.register(Token)
class TokenAdmin(TimeStampedAdmin):
    list_display = ('__str__', 'bankprofile', 'created_at', 'updated_at')


@admin.register(BankProfile)
class BankProfileAdmin(TimeStampedAdmin):
    list_display = 'name', 'user', 'token', 'provider', 'created_at', 'updated_at'


@admin.register(BankAccount)
class BankAccountAdmin(TimeStampedAdmin):
    list_display = ('display_name', 'bank_profile', 'account_id', 'currency', 'swift_bic', 'number')


@admin.register(AccountBalance)
class AccountBalanceAdmin(TimeStampedAdmin):
    list_display = ('bank_account', 'currency', 'available', 'current', 'overdraft', 'update_timestamp')


@admin.register(CardBalance)
class CardBalanceAdmin(TimeStampedAdmin):
    list_display = ('credit_card', 'currency', 'available', 'current', 'credit_limit', 'update_timestamp')


@admin.register(CreditCard)
class CreditCardAdmin(TimeStampedAdmin):
    list_display = ('display_name', 'bank_profile', 'account_id', 'currency', 'partial_card_number', 'name_on_card')


@admin.register(Provider)
class ProviderAdmin(TimeStampedAdmin):
    list_display = ('display_name', 'provider_id')


@admin.register(AsyncRequest)
class AsyncRequestAdmin(TimeStampedAdmin):
    list_display = 'task_id', 'request_type', 'status', 'created_at', 'updated_at', 'error'


@admin.register(Transaction)
class TransactionAdmin(TimeStampedAdmin):
    list_display = 'transaction_id', 'bank_account', 'currency', 'amount', 'created_at', 'updated_at', 'timestamp'
