import time
import logging
import pytz
import requests
import urllib
from datetime import datetime, timedelta
from django.conf import settings
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.filters import OrderingFilter
from rest_framework.generics import ListAPIView, RetrieveUpdateAPIView
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from django_filters import rest_framework as filters
from authentication.models import CustomUser

from truelayer.exceptions import ResponseValidationError
from truelayer.models import AsyncRequest, BankProfile, BankAccount, Transaction, CreditCard
from truelayer.requests import send_request_to_true_layer
from truelayer.serializers import (TokenSerializer, CreateTokenSerializer, TransactionSerializer,
                                   BankAccountSerializer, CreateUpdateBankProfileSerilizer,
                                   CreditCardSerializer, UpdateCreditCardSerializer,
                                   UpdateAsyncRequestSerializer)
from truelayer.tasks import (process_request_to_true_layer,
                             request_balance, request_balance_for_card, request_accounts, request_credit_cards,
                             request_async_transactions, request_transactions_for_account,
                             update_user_accounts_and_balances, update_user_cards_and_balances)
from truelayer.utils import get_redirect_uri

logger = logging.getLogger(__name__)


@api_view(['GET'])
def true_layer_connect(request):
    """Retrieve a link to sign in to True Layer"""
    def _get_authentication_link(state):
        params = {
            "response_type": "code",
            "client_id": settings.TRUELAYER_CLIENT_ID,
            "nonce": int(time.time()),
            # List of permissions https://docs.truelayer.com/#permissions
            "scope": "info cards accounts transactions balance offline_access direct_debits standing_orders products beneficiaries",
            "redirect_uri": get_redirect_uri(),
            "enable_mock": "true",
            "enable_oauth_providers": "true",
            "enable_open_banking_providers": "true",
            "enable_credentials_sharing_providers": "true",
            # "disable_providers": already_connected_providers()
            "state": state
        }
        query = urllib.parse.urlencode(params, quote_via=urllib.parse.quote, safe='/:')
        return settings.TRUELAYER_AUTH_URI + "/?" + query
    connect_links = {'new': _get_authentication_link(f'user_{request.user.id}')}
    for profile in request.user.bank_profiles.all():
        connect_links[f'profile_{profile.id}_{profile.name}'] = _get_authentication_link(f'profile_{profile.id}')
    return Response(connect_links)


def exchange_code_with_access_token(code):
    # Exchange code for access_token and request meta data to define provider
    data = {
        'client_id': settings.TRUELAYER_CLIENT_ID,
        'client_secret': settings.TRUELAYER_CLIENT_SECRET,
        'code': code,
        'grant_type': "authorization_code",
        'redirect_uri': get_redirect_uri()
    }
    response = requests.post(settings.TRUELAYER_TOKEN_URL, data=data)
    if response.status_code != 200:
        raise ResponseValidationError(msg='Failed to exchange code for access_token.',
                                      response=response, status=response.status_code,
                                      errors=f"Status code {response.status_code}")
    serializer = TokenSerializer(data=response.json())
    if not serializer.is_valid():
        raise ResponseValidationError(msg='Failed to exchange code for access_token.',
                                      response=response, status=response.status_code,
                                      errors=serializer.errors)
    return response


def request_access_token_meta_data(access_token):
    ''' Make request for the access_token meta data to define the provider'''
    url = settings.TRUELAYER_TOKEN_META_URL
    headers = {'Authorization': "Bearer " + access_token}
    r = requests.get(url, headers=headers)
    if r.status_code != 200 or len(r.json().get('results', [])) == 0 or 'provider' not in r.json()['results'][0].keys():
        raise ResponseValidationError(msg=f"Failed to get provider for access_token {access_token}",
                                      response=r, status=r.status_code,
                                      errors=None)
    return r


def obtain_access_token(code, state):
    '''Save new access_token (add new profile)
    or save token for existing profile (re-connect profile)
    '''
    token_response = exchange_code_with_access_token(code).json()
    meta_response = request_access_token_meta_data(token_response['access_token'])

    token_response['raw_response'] = meta_response.json()
    profile_data = {
        'token': token_response,
        'provider': meta_response.json()['results'][0]['provider'],
    }
    if state.startswith('user_'):  # create new profile and new token
        profile = None
        profile_data.update({'user': state.strip('user_')})

    elif state.startswith('profile_'):  # update profile and its token
        profile = BankProfile.objects.get(id=state.strip('profile_'))
        profile_data.update({'user': profile.user_id})
    else:
        raise Exception('Incorrect state! {state}')

    serializer = CreateUpdateBankProfileSerilizer(instance=profile, data=profile_data)
    if not serializer.is_valid():
        raise ResponseValidationError(msg=f'Failed to obtain access_token for state {state}',
                                      response=meta_response, status=500,
                                      errors=serializer.errors)
    profile = serializer.save()
    return profile.token


class TrueLayerRedirectUriAPI(APIView):
    '''Receive code from True Layer after user authenticate.
    Exchange code for access_token and gather info on user accounts.
    '''
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'authentication_complete.html'
    permission_classes = (AllowAny, )

    def get(self, request):
        code = request.query_params['code']
        state = request.query_params['state']
        token = obtain_access_token(code, state)
        response_info = send_request_to_true_layer(
            url=settings.TRUELAYER_INFO_URL, token_id=token.id)
        request_accounts(token)
        request_credit_cards(token)

        accounts = token.bankprofile.bank_accounts.all()
        cards = token.bankprofile.credit_cards.all()
        for a in accounts:
            request_balance(token=token, bank_account=a)
            request_async_transactions(a.account_id, token.id, period='month')
            request_async_transactions(a.account_id, token.id, period='year')

        for c in cards:
            request_balance_for_card(token=token, credit_card=c)

        return Response({
            'success': True,
            'token': {'access_token': token.access_token, 'updated_at': token.updated_at},
            'info': response_info.json(),
            'accounts': BankAccountSerializer(accounts, many=True).data
        })


class TrueLayerWebhookUriAPI(APIView):
    '''The URI where notification of completion will be sent via HTTP POST.
    Requests, that expect time-consuming responses are instead sent with async=true.
    POST request to the webhook uri is expected when data is processed from True Layer
    and could be retrieved on the specified results_uri.
    We track the received data by updating the AsyncRequest record with that task_id
    '''
    permission_classes = (AllowAny, )

    def post(self, request):
        if 'task_id' not in request.data.keys():
            raise Exception('True Layer called back but task_id is missing. POST data' + str(request.data))
        task_id = request.data['task_id']
        try:
            async_request = AsyncRequest.objects.get(task_id=task_id)
        except AsyncRequest.DoesNotExist:
            logger.error(
                f"True Layer called back but AsyncRequest record with task_id {task_id} does not exist.")
            return Response(status=status.HTTP_200_OK)
        serializer = UpdateAsyncRequestSerializer(
            instance=async_request, data=request.data)
        serializer.is_valid(raise_exception=True)
        request_to_process = serializer.save()
        logger.info('True Layer called back. Task id: ' + task_id)
        if request_to_process.status == AsyncRequest.STATUS_SUCCEEDED:
            process_request_to_true_layer(async_request_id=async_request.id)
        else:
            logger.error(f"Async Request with task_id {task_id} did not succeed."
                         f"Status updated to Failed. Data received: {request.data}")
        return Response(status=status.HTTP_200_OK)


class BankAccountsListView(ListAPIView):
    serializer_class = BankAccountSerializer

    def get_queryset(self):
        return BankAccount.objects.filter(bank_profile__user=self.request.user, status=BankAccount.STATUS_VISIBLE)


class BankAccountsListRefreshView(BankAccountsListView):
    def get(self, request, *args, **kwargs):
        update_user_accounts_and_balances(user_id=self.request.user.id)
        return super().get(request, *args, **kwargs)


class BankAccountUpdateView(RetrieveUpdateAPIView):
    serializer_class = BankAccountSerializer

    def get_queryset(self):
        return BankAccount.objects.filter(bank_profile__user=self.request.user)


class CreditCardsListView(ListAPIView):
    serializer_class = CreditCardSerializer

    def get_queryset(self):
        return CreditCard.objects.filter(bank_profile__user=self.request.user, status=CreditCard.STATUS_VISIBLE)


class CreditCardsListRefreshView(CreditCardsListView):
    def get(self, request, *args, **kwargs):
        update_user_cards_and_balances(user_id=self.request.user.id)
        return super().get(request, *args, **kwargs)


class CreditCardUpdateView(RetrieveUpdateAPIView):
    serializer_class = UpdateCreditCardSerializer

    def get_queryset(self):
        return CreditCard.objects.filter(bank_profile__user=self.request.user)


class DateRangeFilter(filters.FilterSet):
    start = filters.IsoDateTimeFilter(
        field_name='timestamp', lookup_expr='gte')
    end = filters.IsoDateTimeFilter(field_name='timestamp', lookup_expr='lte')

    class Meta:
        model = Transaction
        fields = ['start', 'end']


class TransactionsListView(ListAPIView):
    '''Get account transactions stored in our db.
    Optional params: start, end (iso time stamp)
    '''
    serializer_class = TransactionSerializer
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filterset_class = DateRangeFilter
    ordering_fields = ('timestamp', )

    def _validate_account(self):
        '''Check if account exists and belongs to the request user'''
        self.account = get_object_or_404(BankAccount,
                                         id=self.kwargs['account_pk'],
                                         bank_profile__user=self.request.user)

    def get(self, request, *args, **kwargs):
        self._validate_account()
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return Transaction.objects.filter(bank_account_id=self.kwargs['account_pk'])


class TransactionsListRefreshView(TransactionsListView):
    '''Make request to True Layer to update db with existing transactions for account.
    Then return account transactions stored in our db.
    Optional params: start, end (iso time stamp)
    '''
    def get(self, request, *args, **kwargs):
        self._validate_account()
        url_params = {
            'from': (datetime.now(tz=pytz.utc) - timedelta(days=8)).isoformat(),
            'to': datetime.now(tz=pytz.utc).isoformat()
        }
        request_transactions_for_account(account_id=self.account.account_id,
                                         token=self.account.bank_profile.token,
                                         params=url_params)
        return super(TransactionsListView, self).get(request, *args, **kwargs)
