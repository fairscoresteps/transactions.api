import pytz
from datetime import datetime
from model_mommy.recipe import Recipe, foreign_key
from truelayer.models import (Token, Provider, AccountBalance, CardBalance, Transaction,
                              BankProfile, AsyncRequest)


provider = Recipe(Provider,
                  display_name="Bank 1",
                  provider_id="bank_1",
                  logo_uri="https://test.com/logo.svg")
token = Recipe(Token,
               updated_at=datetime.now(tz=pytz.utc),
               expires_in=3600)

profile = Recipe(BankProfile,
                 provider=foreign_key('provider'),
                 token=foreign_key('token'))

balance = Recipe(AccountBalance,
                 available=125.0,
                 currency="GBP",
                 current=25.0,
                 overdraft=100.0)

card_balance = Recipe(CardBalance,
                      available=80.0,
                      currency="GBP",
                      current=40.0,
                      credit_limit=120.0,
                      last_statement_balance=4.0,
                      payment_due=5.0)

transaction_debit = Recipe(Transaction,
                           transaction_id='abcdefghigklmnopqrstuvwxyz123456',
                           transaction_type='DEBIT',
                           transaction_category='PURCHASE',
                           amount=-20,
                           currency="GBP")

transactions_credit = Recipe(Transaction,
                             transaction_id='123456abcdefghigklmnopqrstuvwxyz',
                             transaction_type='CREDIT',
                             transaction_category='TRANSFER',
                             amount=20,
                             currency="GBP")

async_record_accounts = Recipe(AsyncRequest,
                               request_type=AsyncRequest.TYPE_ACCOUNTS,
                               status=AsyncRequest.STATUS_QUEUED,
                               token=foreign_key(token),
                               extra_data={})
