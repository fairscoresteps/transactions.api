from django.db import transaction
from rest_framework import serializers

from truelayer.models import (Token, BankProfile, BankAccount, Provider, Transaction, CreditCard,
                              AccountBalance, CardBalance, AsyncRequest)


class TokenSerializer(serializers.ModelSerializer):

    class Meta:
        model = Token
        fields = 'access_token', 'refresh_token', 'expires_in', 'token_type'


class ProviderSerializer(serializers.ModelSerializer):
    provider_id = serializers.CharField()  # no unique constraint

    class Meta:
        model = Provider
        fields = 'display_name', 'provider_id', 'logo_uri'


class CreateTokenSerializer(TokenSerializer):
    raw_response = serializers.JSONField()

    class Meta:
        model = Token
        fields = 'access_token', 'refresh_token', 'expires_in', 'token_type', 'raw_response'


class UpdateTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = 'access_token', 'refresh_token', 'expires_in', 'token_type'


class CreateUpdateBankProfileSerilizer(serializers.ModelSerializer):
    provider = ProviderSerializer()
    token = CreateTokenSerializer()

    class Meta:
        model = BankProfile
        fields = 'name', 'provider', 'user', 'token'

    def create(self, validated_data):
        # Create new profile with token
        provider_data = validated_data.pop('provider')
        token_data = validated_data.pop('token')
        provider, created = Provider.objects.update_or_create(provider_id=provider_data['provider_id'],
                                                              defaults=provider_data)
        token = Token.objects.create(**token_data)
        data = {
            'name': provider.display_name,
            'provider': provider,
            'token': token
        }
        data.update(validated_data)
        profile = BankProfile.objects.create(**data)
        return profile

    def update(self, instance, validated_data):
        # Delete the old token and accounts. Save new token.
        with transaction.atomic():
            provider_data = validated_data.pop('provider')
            token_data = validated_data.pop('token')
            provider, created = Provider.objects.update_or_create(provider_id=provider_data['provider_id'],
                                                                  defaults=provider_data)
            instance.provider = provider
            instance.name = provider.display_name
            old_token = instance.token
            instance.token = Token.objects.create(**token_data)
            old_token.delete()
            instance.bank_accounts.all().delete()
            instance.save()
        return instance


class BankAccountNumberHelperSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        fields = 'swift_bic', 'number', 'sort_code'


class CreateBankAccountSerializer(serializers.ModelSerializer):
    '''Overriding the default account_id field without unique=True constraint.
    So instead of raising "Already exists" when calling is_valid()
    Accounts that already exist will be skipped when calling create().
    '''
    account_id = serializers.CharField()  # no unique constraint
    account_number = BankAccountNumberHelperSerializer()
    provider = ProviderSerializer()

    class Meta:
        model = BankAccount
        fields = ('update_timestamp',
                  'account_id',
                  'account_type',
                  'display_name',
                  'currency',
                  'account_number',
                  'provider')

    def create(self, validated_data):
        # flatten nested provider and account number to match the model fields
        provider_data = validated_data.pop('provider')
        provider, created = Provider.objects.get_or_create(provider_id=provider_data['provider_id'],
                                                           defaults=provider_data)
        account_data = {'provider': provider}
        account_data.update(validated_data.pop('account_number'))
        account_data.update(validated_data)
        obj, created = BankAccount.objects.update_or_create(account_id=account_data['account_id'],
                                                            bank_profile=self.context.get('bank_profile'),
                                                            defaults=account_data)
        return obj


class BalanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountBalance
        fields = '__all__'

    def create(self, validated_data):
        obj, created = AccountBalance.objects.update_or_create(
            update_timestamp=validated_data['update_timestamp'], defaults=validated_data)
        return obj


class CardBalanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = CardBalance
        fields = '__all__'

    def create(self, validated_data):
        obj, created = CardBalance.objects.update_or_create(
            update_timestamp=validated_data['update_timestamp'], defaults=validated_data)
        return obj


class BankAccountSerializer(serializers.ModelSerializer):
    balance = BalanceSerializer(read_only=True)
    provider = ProviderSerializer(read_only=True)
    update_timestamp = serializers.DateTimeField(read_only=True)
    account_id = serializers.CharField(read_only=True)
    account_type = serializers.CharField(read_only=True)
    display_name = serializers.CharField(read_only=True)
    currency = serializers.CharField(read_only=True)
    swift_bic = serializers.CharField(read_only=True)
    number = serializers.CharField(read_only=True)
    sort_code = serializers.CharField(read_only=True)
    bank_profile = CreateUpdateBankProfileSerilizer(read_only=True)
    status = serializers.ChoiceField(BankAccount.STATUS_CHOICES)

    class Meta:
        model = BankAccount
        fields = '__all__'


class CreditCardSerializer(serializers.ModelSerializer):
    balance = CardBalanceSerializer()
    provider = ProviderSerializer()

    class Meta:
        model = CreditCard
        fields = '__all__'


class CreateCreditCardSerializer(serializers.ModelSerializer):
    provider = ProviderSerializer()

    class Meta:
        model = CreditCard
        fields = ('account_id',
                  'account_type',
                  'card_network',
                  'card_type',
                  'currency',
                  'display_name',
                  'partial_card_number',
                  'name_on_card',
                  'valid_from',
                  'valid_to',
                  'update_timestamp',
                  'provider')

    def create(self, validated_data):
        # flatten nested provider to match the model fields
        provider_data = validated_data.pop('provider')
        provider, created = Provider.objects.get_or_create(provider_id=provider_data['provider_id'],
                                                           defaults=provider_data)
        card_data = {'provider': provider}
        card_data.update(validated_data)
        obj, created = CreditCard.objects.update_or_create(account_id=card_data['account_id'],
                                                           bank_profile=self.context.get('bank_profile'),
                                                           defaults=card_data)
        return obj


class UpdateCreditCardSerializer(serializers.ModelSerializer):

    class Meta:
        model = CreditCard
        fields = 'status',


class CreateTransactionSerializer(serializers.ModelSerializer):
    transaction_id = serializers.CharField()  # no unique constraint
    transaction_classification = serializers.JSONField()
    meta = serializers.JSONField()

    class Meta:
        model = Transaction
        fields = ('transaction_id', 'amount', 'currency', 'timestamp',
                  'transaction_type', 'transaction_category', 'transaction_classification',
                  'description', 'merchant_name', 'meta')

    def create(self, validated_data):
        validated_data['bank_account_id'] = self.context.get('bank_account_id')
        obj, created = Transaction.objects.update_or_create(transaction_id=validated_data.pop('transaction_id'),
                                                            bank_account_id=validated_data['bank_account_id'],
                                                            defaults=validated_data)
        return obj


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = '__all__'


class CreateAsyncRequestSerializer(serializers.ModelSerializer):
    extra_data = serializers.JSONField()

    class Meta:
        model = AsyncRequest
        fields = 'task_id', 'token', 'status', 'results_uri', 'request_type', 'extra_data'


class UpdateAsyncRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = AsyncRequest
        fields = ('status', 'results_uri', 'request_uri', 'credentials_id',
                  'request_timestamp', 'error_description', 'error')
