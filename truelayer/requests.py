
import logging
import pytz
import requests
from datetime import datetime, timedelta
from django.conf import settings
from django.db import transaction
from rest_framework import status

from truelayer.exceptions import ResponseValidationError
from truelayer.models import AsyncRequest, Token
from truelayer.serializers import UpdateTokenSerializer, CreateAsyncRequestSerializer
from truelayer.utils import get_webhook_uri

logger = logging.getLogger(__name__)


@transaction.atomic()
def renew_expired_access_token(token_id):
    '''Update token if expired already or expires soon. Return the old one otherwise.'''
    token = Token.objects.select_for_update().get(id=token_id)
    if token.expires_soon:
        response = requests.post(settings.TRUELAYER_TOKEN_URL, data={
            'grant_type': 'refresh_token',
            'client_id': settings.TRUELAYER_CLIENT_ID,
            'client_secret': settings.TRUELAYER_CLIENT_SECRET,
            'refresh_token': token.refresh_token
        })
        if response.status_code == 200:
            serializer = UpdateTokenSerializer(instance=token, data=response.json())
            if not serializer.is_valid():
                raise ResponseValidationError(msg='Failed to validate renewed access_token.',
                                              response=response, status=response.status_code,
                                              errors=serializer.errors)
            token = serializer.save()  # update existing token instance with new data
        else:
            logger.critical(f"Failed to renew expired token {token.id}."
                            f"Resposne {response.status_code}: {response.json()}")
    return token


def send_request_to_true_layer(url, token_id, params=None):
    token = renew_expired_access_token(token_id)
    headers = {'Authorization': "Bearer " + token.access_token}
    response = requests.get(url, headers=headers, params=params)
    return response


def send_async_request_to_true_layer(url, token_id, request_type, url_params=None, extra_data=None):
    '''
    Send async request to True Layer for data that's usually slow to proccess on their end.
    Keep the task_id from their response (in AsyncResponse record).
    When True Layer has the data prepared, they'll notify us on webhook_ur
    so we can make request
    '''
    url_params = url_params or {}
    url_params.update({'async': 'true', 'webhook_uri': get_webhook_uri()})

    response = send_request_to_true_layer(url, token_id, url_params)
    if response.status_code not in [status.HTTP_200_OK, status.HTTP_202_ACCEPTED]:
        logger.error(f"Async Request to True Layer failed with status {response.status_code}."
                     f"URL: {response.request.path_url} Response: {response.json()}")
        return
    data = {'token': token_id, 'request_type': request_type, 'extra_data': extra_data or {}}
    data.update(response.json())
    serializer = CreateAsyncRequestSerializer(data=data)
    if not serializer.is_valid():
        raise ResponseValidationError(msg='Failed to initiate async request.',
                                      response=response, status=response.status_code,
                                      errors=serializer.errors)
    sent = serializer.save()
    logger.info(f"Creating async request record of type {request_type} Task_id: {sent.task_id}")


def test_async_transactions():
    from truelayer.models import BankAccount
    a = BankAccount.objects.latest('pk')
    t = a.token_id
    logger.info('Testing transactions')
    send_async_request_to_true_layer(url=settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=a.account_id),
                                     token_id=t, request_type=AsyncRequest.TYPE_TRANSACTIONS,
                                     extra_data={'account_id': a.account_id})
