# Generated by Django 2.2.1 on 2019-05-30 09:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('truelayer', '0003_creditcard'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountBalance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('currency', models.CharField(max_length=3)),
                ('available', models.DecimalField(decimal_places=2, max_digits=15)),
                ('current', models.DecimalField(decimal_places=2, max_digits=15)),
                ('update_timestamp', models.DateTimeField()),
                ('overdraft', models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CardBalance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('currency', models.CharField(max_length=3)),
                ('available', models.DecimalField(decimal_places=2, max_digits=15)),
                ('current', models.DecimalField(decimal_places=2, max_digits=15)),
                ('update_timestamp', models.DateTimeField()),
                ('credit_limit', models.DecimalField(decimal_places=2, max_digits=15)),
                ('last_statement_balance', models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True)),
                ('last_statement_date', models.DateTimeField()),
                ('payment_due', models.DecimalField(blank=True, decimal_places=2, max_digits=15, null=True)),
                ('payment_due_date', models.DateTimeField(blank=True, null=True)),
                ('credit_card', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='balances', to='truelayer.CreditCard')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='bankaccount',
            name='balance',
        ),
        migrations.DeleteModel(
            name='Balance',
        ),
        migrations.AddField(
            model_name='accountbalance',
            name='bank_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='balances', to='truelayer.BankAccount'),
        ),
    ]
