from django.apps import AppConfig


class TrueLayerConfig(AppConfig):
    name = 'truelayer'
