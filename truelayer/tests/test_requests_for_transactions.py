import datetime
import pytz
import mock
import responses
from decimal import Decimal
from django.test import TestCase
from django.conf import settings
from django.urls import reverse
from freezegun import freeze_time
from model_mommy import mommy

from truelayer.models import BankAccount, Transaction
from truelayer.tasks import request_transactions_for_account
from truelayer.tests.example_responses import simulate_response_transactions
from truelayer.tests.test_basic import AuthenticatedTestCase


@mock.patch('truelayer.tasks.logger')
class TestRequestTransactionsFromTrueLayer(TestCase):
    def setUp(self):
        self.token = mommy.make_recipe('truelayer.token')
        self.profile = mommy.make_recipe('truelayer.profile', token=self.token)
        self.provider = self.profile.provider
        self.account = mommy.make(BankAccount, bank_profile=self.profile, provider=self.provider)

    @responses.activate
    def test_get_transactions(self, mocked_logger):
        assert self.account.transactions.count() == 0
        responses.add(responses.GET,
                      settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=self.account.account_id),
                      json=simulate_response_transactions(), status=200)

        request_transactions_for_account(self.account.account_id, self.token)
        assert self.account.transactions.count() == 22
        mocked_logger.error.assert_not_called()

    @responses.activate
    def test_get_transactions_no_results(self, mocked_logger):
        assert self.account.transactions.count() == 0
        responses.add(responses.GET,
                      settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=self.account.account_id),
                      json=simulate_response_transactions(empty=True), status=200)

        request_transactions_for_account(self.account.account_id, self.token)
        assert self.account.transactions.count() == 0
        mocked_logger.error.assert_not_called()

    @responses.activate
    def test_get_transactions_no_results_existing_ones_are_kept(self, mocked_logger):
        mommy.make_recipe('truelayer.transactions_credit', bank_account=self.account)
        assert self.account.transactions.count() == 1
        responses.add(responses.GET,
                      settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=self.account.account_id),
                      json=simulate_response_transactions(empty=True), status=200)

        request_transactions_for_account(self.account.account_id, self.token)
        assert self.account.transactions.count() == 1
        mocked_logger.error.assert_not_called()

    @responses.activate
    def test_result_records_after_successful_request(self, mocked_logger):
        mommy.make_recipe('truelayer.transactions_credit', bank_account=self.account)
        assert self.account.transactions.count() == 1
        responses.add(responses.GET,
                      settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=self.account.account_id),
                      json=simulate_response_transactions(results_count=1), status=200)
        request_transactions_for_account(self.account.account_id, self.token)
        assert self.account.transactions.count() == 2
        new_one = self.account.transactions.get(transaction_id='qwerty1234567890qwerty1234567890')
        assert new_one.amount == Decimal('-7.99')
        assert new_one.currency == "GBP"
        assert new_one.transaction_type == 'DEBIT'
        assert new_one.transaction_category == 'PURCHASE'
        assert new_one.merchant_name == 'AMAZON PRIME'
        assert new_one.description == 'AMAZON PRIME'
        assert new_one.meta is not None

    @responses.activate
    def test_response_status_not_200_no_effect_on_existing(self, mocked_logger):
        mommy.make_recipe('truelayer.transactions_credit', bank_account=self.account)
        assert self.account.transactions.count() == 1
        responses.add(responses.GET,
                      settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=self.account.account_id),
                      json={}, status=400)

        request_transactions_for_account(self.account.account_id, self.token)
        assert self.account.transactions.count() == 1
        mocked_logger.error.called_once()

    @responses.activate
    def test_update_existing_transaction(self, mocked_logger):
        # seems highly unlikely but that's how we currently handle it - by updating existing record
        with freeze_time("2019-01-01 09:00"):
            t = mommy.make_recipe('truelayer.transactions_credit',
                                  bank_account=self.account,
                                  transaction_id='qwerty1234567890qwerty1234567890',
                                  transaction_category='OLD CATEGORY',
                                  description='OLD DESCRIPTION')
        assert self.account.transactions.count() == 1
        assert t.updated_at == datetime.datetime(2019, 1, 1, 9, 0, tzinfo=pytz.utc)

        responses.add(responses.GET,
                      settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=self.account.account_id),
                      json=simulate_response_transactions(results_count=1), status=200)

        with freeze_time("2019-01-01 19:00"):
            request_transactions_for_account(self.account.account_id, self.token)
        assert self.account.transactions.count() == 1
        updated = Transaction.objects.get(id=t.id)
        assert updated.transaction_category == 'PURCHASE'
        assert updated.description == 'AMAZON PRIME'
        assert updated.updated_at == datetime.datetime(2019, 1, 1, 19, 0, tzinfo=pytz.utc)
        mocked_logger.error.assert_not_called()


class ListAccountTransactions(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.profile = mommy.make_recipe('truelayer.profile', user=self.user)
        self.account = mommy.make(BankAccount, bank_profile=self.profile)
        self.url = reverse('account_transactions', kwargs={'account_pk': self.account.id})

    def test_unauthorized(self):
        self.client.force_authenticate(user=None)
        r = self.client.get(self.url)
        assert r.status_code == 401

    def test_no_transactions_for_account(self):
        r = self.client.get(self.url)
        assert r.status_code == 200
        assert len(r.json()) == 0

    def test_get_list_of_transactions_for_account(self):
        mommy.make_recipe('truelayer.transaction_debit', bank_account=self.account)
        mommy.make_recipe('truelayer.transactions_credit', bank_account=self.account)
        r = self.client.get(self.url)
        assert r.status_code == 200
        assert len(r.json()) == 2

    def test_list_of_transactions_exclude_other_users_transactions(self):
        mommy.make(Transaction, bank_account=self.account)
        mommy.make(Transaction, bank_account=self.account)
        # another user's transaction
        user2_transaction = mommy.make(Transaction,
                                       bank_account=mommy.make(BankAccount))

        r = self.client.get(self.url)
        assert r.status_code == 200
        assert len(r.json()) == 2
        transaction_ids = [t['transaction_id'] for t in r.json()]
        assert user2_transaction.transaction_id not in transaction_ids


class RefreshListOfAccountTransactions(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.profile = mommy.make_recipe('truelayer.profile', user=self.user)
        self.account = mommy.make(BankAccount, bank_profile=self.profile)
        self.url = reverse('force_refresh_transactions',
                           kwargs={'account_pk': self.account.id})

    @responses.activate
    def test_request_transactions_before_response(self):
        transaction = mommy.make(Transaction, bank_account=self.account)
        assert self.account.transactions.count() == 1
        responses.add(responses.GET,
                      settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=self.account.account_id),
                      json=simulate_response_transactions(), status=200)
        r = self.client.get(self.url)
        assert r.status_code == 200
        assert len(r.json()) == 23
        assert self.account.transactions.count() == 23
        assert transaction in self.account.transactions.all()

    @responses.activate
    def test_request_transactions_of_someone_elses_account(self):
        transaction = mommy.make(Transaction, bank_account=self.account)
        user2_account = mommy.make(BankAccount)
        self.client.force_authenticate(user=user2_account.bank_profile.user)
        r = self.client.get(self.url)
        assert r.status_code == 404
        assert self.account.transactions.count() == 1
        assert transaction in self.account.transactions.all()

    @responses.activate
    def test_request_transactions_returned_zero_results(self):
        assert self.account.transactions.count() == 0
        responses.add(responses.GET,
                      settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=self.account.account_id),
                      json=simulate_response_transactions(empty=True), status=200)
        r = self.client.get(self.url)
        assert r.status_code == 200
        assert len(r.json()) == 0
        assert self.account.transactions.count() == 0
