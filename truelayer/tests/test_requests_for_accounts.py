import mock
import responses

from django.test import TestCase
from django.conf import settings
from django.urls import reverse
from model_mommy import mommy

from authentication.models import CustomUser
from truelayer.models import BankAccount, Provider
from truelayer.tasks import request_accounts
from truelayer.tests.example_responses import (simulate_response_for_accounts,
                                               simulate_response_for_balance)
from truelayer.tests.test_basic import AuthenticatedTestCase


@mock.patch('truelayer.tasks.logger')
class TestRequestAccountsFromTrueLayer(TestCase):
    def setUp(self):
        self.token = mommy.make_recipe('truelayer.token')
        self.profile = mommy.make_recipe('truelayer.profile', token=self.token)
        self.provider = self.profile.provider
        self.existing_account = mommy.make(BankAccount, bank_profile=self.profile, provider=self.provider)

    @responses.activate
    def test_add_more_accounts(self, mocked_logger):
        assert self.profile.bank_accounts.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json=simulate_response_for_accounts(), status=200)

        request_accounts(self.token)
        assert self.profile.bank_accounts.count() == 6
        assert self.existing_account in self.profile.bank_accounts.all()
        mocked_logger.error.assert_not_called()

    @responses.activate
    def test_no_accounts(self, mocked_logger):
        self.existing_account.delete()
        assert self.profile.bank_accounts.count() == 0
        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json=simulate_response_for_accounts(empty=True), status=200)

        request_accounts(self.token)
        assert self.profile.bank_accounts.count() == 0
        mocked_logger.error.assert_not_called()

    @responses.activate
    def test_no_accounts_received_existing_ones_are_kept(self, mocked_logger):
        assert self.profile.bank_accounts.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json=simulate_response_for_accounts(empty=True), status=200)

        request_accounts(self.token)
        assert self.profile.bank_accounts.count() == 1
        assert self.existing_account in self.profile.bank_accounts.all()
        mocked_logger.error.assert_not_called()

    @responses.activate
    def test_response_status_not_200_no_effect_on_existing(self, mocked_logger):
        assert self.profile.bank_accounts.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json={}, status=400)
        request_accounts(self.token)
        assert self.profile.bank_accounts.count() == 1
        assert self.existing_account in self.profile.bank_accounts.all()
        mocked_logger.error.assert_called_once()

    @responses.activate
    def test_response_error_no_effect_on_existing(self, mocked_logger):
        assert self.profile.bank_accounts.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json=simulate_response_for_accounts(error=True), status=500)
        request_accounts(self.token)
        assert self.profile.bank_accounts.count() == 1
        assert self.existing_account in self.profile.bank_accounts.all()
        mocked_logger.error.assert_called_once()

    @responses.activate
    def test_missing_results_key_in_response(self, mocked_logger):
        assert self.profile.bank_accounts.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json={}, status=200)

        request_accounts(self.token)
        assert self.profile.bank_accounts.count() == 1
        assert self.existing_account in self.profile.bank_accounts.all()
        mocked_logger.error.assert_called_once()

    @responses.activate
    def test_result_records_after_successful_request(self, mocked_logger):
        self.existing_account.delete()
        assert self.profile.bank_accounts.count() == 0
        existing_provider = self.profile.provider
        assert Provider.objects.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json=simulate_response_for_accounts(), status=200)

        request_accounts(self.token)
        # new provider appeared in the accounts data, so it's stored
        assert Provider.objects.count() == 2
        assert self.profile.bank_accounts.count() == 5
        a1 = self.profile.bank_accounts.get(account_id='account_id_1')
        assert a1.account_type == 'TRANSACTION'
        assert a1.display_name == 'TRANSACTION ACCOUNT 1'
        assert a1.currency == 'GBP'
        assert a1.swift_bic == 'SWIFTBIC'
        assert a1.number == '10000000'
        assert a1.sort_code == '01-21-31'
        assert a1.balance is None
        assert a1.provider.display_name == 'Mock'
        assert a1.provider.provider_id == 'mock'
        assert a1.provider.logo_uri == 'https://test.com/mock-bank-icon.svg'
        # even if accounts were sent with different provider data, token provider remains the same
        assert a1.provider.id != existing_provider.id
        mocked_logger.error.assert_not_called()

    @responses.activate
    def test_request_for_accounts_does_not_udate_existing_provider_data_when_same_provider_id(self, mocked_logger):
        provider = self.profile.provider
        provider.provider_id = "mock"
        provider.save()

        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json=simulate_response_for_accounts(), status=200)
        request_accounts(self.token)
        a1 = self.profile.bank_accounts.get(account_id='account_id_1')
        assert a1.provider.id == self.profile.provider.id
        assert a1.provider.display_name == "Bank 1"  # despite response says display_name is "Mock"


class ListAccountsForUser(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.profile = mommy.make_recipe('truelayer.profile', user=self.user)
        self.account1 = mommy.make(BankAccount, bank_profile=self.profile)
        self.account2 = mommy.make(BankAccount, bank_profile=self.profile)
        self.account3 = mommy.make(BankAccount, bank_profile=self.profile)
        self.another_user_account = mommy.make(BankAccount)
        self.url = reverse('user_accounts')

    def test_unauthorized(self):
        self.client.force_authenticate(user=None)
        r = self.client.get(self.url)
        assert r.status_code == 401

    def test_no_accounts_for_user(self):
        self.client.force_authenticate(user=mommy.make(CustomUser))
        r = self.client.get(self.url)
        assert r.status_code == 200
        assert len(r.json()) == 0

    def test_get_list_of_accounts(self):
        r = self.client.get(self.url)
        assert r.status_code == 200
        assert len(r.json()) == 3
        account_ids = [a['account_id'] for a in r.json()]
        assert self.another_user_account.account_id not in account_ids


class RefreshListOfAccountsForUser(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.profile = mommy.make_recipe('truelayer.profile', user=self.user)
        self.account = mommy.make(BankAccount, bank_profile=self.profile)
        self.url = reverse('force_refresh_user_accounts')

    @responses.activate
    def test_request_accounts_before_response(self):
        assert BankAccount.objects.filter(bank_profile__user=self.user).count() == 1
        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json=simulate_response_for_accounts(count=1), status=200)
        responses.add(responses.GET,
                      settings.TRUELAYER_BALANCE_URL.format(account_id=self.account.account_id),
                      json=simulate_response_for_balance(), status=200)  # for the existing account
        responses.add(responses.GET,
                      settings.TRUELAYER_BALANCE_URL.format(account_id='account_id_1'),
                      json=simulate_response_for_balance(), status=200)  # for the new account
        r = self.client.get(self.url)
        assert r.status_code == 200
        assert len(r.json()) == 2
        assert self.profile.bank_accounts.count() == 2
        assert self.account in self.profile.bank_accounts.all()

    @responses.activate
    def test_refresh_accounts_of_someone_else(self):
        user2 = mommy.make(CustomUser)
        self.client.force_authenticate(user=user2)
        r = self.client.get(self.url)
        assert r.status_code == 200
        assert len(r.json()) == 0
        assert BankAccount.objects.filter(bank_profile__user=self.user).count() == 1

    @responses.activate
    def test_refresh_accounts_returned_zero_results(self):
        user2 = mommy.make(CustomUser)
        self.client.force_authenticate(user=user2)
        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json=simulate_response_for_accounts(empty=True), status=200)
        r = self.client.get(self.url)
        assert r.status_code == 200
        assert len(r.json()) == 0
        assert BankAccount.objects.filter(bank_profile__user=user2).count() == 0
