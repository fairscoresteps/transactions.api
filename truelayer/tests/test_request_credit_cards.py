import mock
import responses

from django.test import TestCase
from django.conf import settings
from model_mommy import mommy

from truelayer.models import CreditCard, Provider
from truelayer.tasks import request_credit_cards
from truelayer.tests.example_responses import simulate_response_for_credit_cards


@mock.patch('truelayer.tasks.logger')
class TestRequestCards(TestCase):
    def setUp(self):
        self.token = mommy.make_recipe('truelayer.token')
        self.profile = mommy.make_recipe('truelayer.profile', token=self.token)
        self.provider = self.profile.provider
        self.existing_card = mommy.make(CreditCard, bank_profile=self.profile, provider=self.provider)

    @responses.activate
    def test_add_more_cards(self, mocked_logger):
        assert self.profile.credit_cards.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_CARDS_URL,
                      json=simulate_response_for_credit_cards(), status=200)

        request_credit_cards(self.token)
        assert self.profile.credit_cards.count() == 3
        assert self.existing_card in self.profile.credit_cards.all()
        mocked_logger.error.assert_not_called()

    @responses.activate
    def test_no_cards(self, mocked_logger):
        self.existing_card.delete()
        assert self.profile.credit_cards.count() == 0
        responses.add(responses.GET, settings.TRUELAYER_CARDS_URL,
                      json=simulate_response_for_credit_cards(empty=True), status=200)

        request_credit_cards(self.token)
        assert self.profile.credit_cards.count() == 0
        mocked_logger.error.assert_not_called()

    @responses.activate
    def test_no_cards_received_existing_ones_are_kept(self, mocked_logger):
        assert self.profile.credit_cards.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_CARDS_URL,
                      json=simulate_response_for_credit_cards(empty=True), status=200)

        request_credit_cards(self.token)
        assert self.profile.credit_cards.count() == 1
        assert self.existing_card in self.profile.credit_cards.all()
        mocked_logger.error.assert_not_called()

    @responses.activate
    def test_response_status_not_200_no_effect_on_existing(self, mocked_logger):
        assert self.profile.credit_cards.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_CARDS_URL,
                      json={}, status=400)
        request_credit_cards(self.token)
        assert self.profile.credit_cards.count() == 1
        assert self.existing_card in self.profile.credit_cards.all()
        mocked_logger.error.assert_called_once()

    @responses.activate
    def test_response_error_no_effect_on_existing(self, mocked_logger):
        assert self.profile.credit_cards.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_CARDS_URL,
                      json=simulate_response_for_credit_cards(error=True), status=500)
        request_credit_cards(self.token)
        assert self.profile.credit_cards.count() == 1
        assert self.existing_card in self.profile.credit_cards.all()
        mocked_logger.error.assert_called_once()

    @responses.activate
    def test_missing_results_key_in_response(self, mocked_logger):
        assert self.profile.credit_cards.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_CARDS_URL,
                      json={}, status=200)

        request_credit_cards(self.token)
        assert self.profile.credit_cards.count() == 1
        assert self.existing_card in self.profile.credit_cards.all()
        mocked_logger.error.assert_called_once()

    @responses.activate
    def test_result_records_after_successful_request(self, mocked_logger):
        self.existing_card.delete()
        assert self.profile.credit_cards.count() == 0
        existing_provider = self.profile.provider
        assert Provider.objects.count() == 1
        responses.add(responses.GET, settings.TRUELAYER_CARDS_URL,
                      json=simulate_response_for_credit_cards(), status=200)

        request_credit_cards(self.token)
        # new provider appeared in the accounts data, so it's stored
        assert Provider.objects.count() == 2
        assert self.profile.credit_cards.count() == 2
        c1 = self.profile.credit_cards.get(account_id='card_account_id_1')
        assert c1.account_type == 'TRANSACTION'
        assert c1.display_name == 'CREDIT CARD 1'
        assert c1.currency == 'GBP'
        assert c1.card_type == 'CREDIT'
        assert c1.card_network == 'MASTERCARD'
        assert c1.partial_card_number == '1000'
        assert c1.name_on_card == 'John Doe'
        assert c1.provider.display_name == 'Mock'
        assert c1.provider.provider_id == 'mock'
        assert c1.provider.logo_uri == 'https://test.com/mock-bank-icon.svg'
        # even if accounts were sent with different provider data, token provider remains the same
        assert c1.provider.id != existing_provider.id
        mocked_logger.error.assert_not_called()
