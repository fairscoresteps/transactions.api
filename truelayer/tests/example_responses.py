error_response = {
    'error_description': 'Sorry, we are experiencing technical difficulties. Please try again later.',
    'error': 'internal_server_error',
    'error_details': {}}


def simulate_response_for_balance(empty=False):
    if empty:
        return {
            "status": "Succeeded",
            "results": []
        }
    return {
        "results": [{
            "available": 585.0,
            "currency": "GBP",
            "current": 85.0,
            "overdraft": 500.0,
            "update_timestamp": "2019-05-16T10:30:00.0927089Z"
        }],
        "status": "Succeeded"
    }


def simulate_response_for_card_balance(empty=False):
    if empty:
        return {
            "status": "Succeeded",
            "results": []
        }
    return {
        'results': [{
            'currency': 'GBP',
            'available': 80.0,
            'current': 40.0,
            'credit_limit': 120.0,
            'last_statement_date': '2019-05-06T00:00:00+00:00',
            'last_statement_balance': 4.0,
            'payment_due': 5.0,
            'payment_due_date': '2019-06-01T00:00:00+00:00',
            'update_timestamp': "2019-05-16T10:30:00.0927089Z"
        }],
        'status': 'Succeeded'}


def simulate_response_for_accounts(error=False, empty=False, count=5):
    if error:
        return error_response
    if empty:
        return {
            "status": "Succeeded",
            "results": []
        }
    results = [
        {
            "update_timestamp": "2019-05-15T11:55:21.6865379Z",
            "account_id": "account_id_1",
            "account_type": "TRANSACTION",
            "display_name": "TRANSACTION ACCOUNT 1",
            "currency": "GBP",
            "account_number": {
                "swift_bic": "SWIFTBIC",
                "number": "10000000",
                "sort_code": "01-21-31"
            },
            "provider": {
                "display_name": "Mock",
                "provider_id": "mock",
                "logo_uri": "https://test.com/mock-bank-icon.svg"
            }
        },
        {
            "update_timestamp": "2019-05-15T11:55:21.6865567Z",
            "account_id": "3c6edb9484ecd581dc1cedde8bedb1f1",
            "account_type": "SAVINGS",
            "display_name": "SAVINGS ACCOUNT 1",
            "currency": "GBP",
            "account_number": {
                "swift_bic": "CPBKGB00",
                "number": "20000000",
                "sort_code": "01-21-31"
            },
            "provider": {
                "display_name": "Mock",
                "provider_id": "mock",
                "logo_uri": "https://test.com/mock-bank-icon.svg"
            }
        }, {
            "update_timestamp": "2019-05-15T11:55:21.6865597Z",
            "account_id": "89c3139784a055b9b47998f9dce9122e",
            "account_type": "TRANSACTION",
            "display_name": "TRANSACTION ACCOUNT 2",
            "currency": "GBP",
            "account_number": {
                "swift_bic": "CPBKGB00",
                "number": "30000000",
                "sort_code": "01-21-31"
            },
            "provider": {
                "display_name": "Mock",
                "provider_id": "mock",
                "logo_uri": "https://test.com/mock-bank-icon.svg"
            }
        },
        {
            "update_timestamp": "2019-05-15T11:55:21.686569Z",
            "account_id": "328df3a40b828340fa4c3100e17de121",
            "account_type": "SAVINGS",
            "display_name": "SAVINGS ACCOUNT 2",
            "currency": "GBP",
            "account_number": {
                "swift_bic": "CPBKGB00",
                "number": "40000000",
                "sort_code": "01-21-31"
            },
            "provider": {
                "display_name": "Mock",
                "provider_id": "mock",
                "logo_uri": "https://test.com/mock-bank-icon.svg"
            }
        },
        {
            "update_timestamp": "2019-05-15T11:55:21.6865717Z",
            "account_id": "8de2de9eab01b935b21abcbed11adf26",
            "account_type": "TRANSACTION",
            "display_name": "TRANSACTION ACCOUNT 3",
            "currency": "GBP",
            "account_number": {
                "swift_bic": "CPBKGB00",
                "number": "50000000",
                "sort_code": "01-21-31"
            },
            "provider": {
                "display_name": "Mock",
                "provider_id": "mock",
                "logo_uri": "https://test.com/mock-bank-icon.svg"
            }
        }
    ]
    count = count or len(results)
    return {
        "status": "Succeeded",
        "results": results[:count]
    }


def simulate_response_for_credit_cards(error=None, empty=False):
    if error:
        return error_response
    if empty:
        return {
            "status": "Succeeded",
            "results": []
        }
    return {
        'results': [{
            'account_id': 'card_account_id_1',
            'account_type': 'TRANSACTION',
            'card_network': 'MASTERCARD',
            'card_type': 'CREDIT',
            'currency': 'GBP',
            'display_name': 'CREDIT CARD 1',
            'partial_card_number': '1000',
            'name_on_card': 'John Doe',
            'update_timestamp': '2019-05-29T10:14:59.9994356Z',
            'provider': {
                'display_name': 'Mock',
                'provider_id': 'mock',
                'logo_uri': 'https://test.com/mock-bank-icon.svg'
            }
        }, {
            'account_id': 'card_account_id_2',
            'account_type': 'TRANSACTION',
            'card_network': 'VISA',
            'card_type': 'CREDIT',
            'currency': 'GBP',
            'display_name': 'CREDIT CARD 2',
            'partial_card_number': '2000',
            'name_on_card': 'John Doe',
            'update_timestamp': '2019-05-29T10:14:59.9994511Z',
            'provider': {
                'display_name': 'Mock',
                'provider_id': 'mock',
                'logo_uri': 'https://test.com/mock-bank-icon.svg'
            }
        }], 'status': 'Succeeded'}


def simulate_response_exchange_code_with_access_token():
    '''What we expect to receive in POST request from True Layer
    to our redirect uri after sucessful code exchange with post to TRUELAYER_TOKEN_URL
    '''

    return {
        'access_token': '1234567890qwertyuiopasdfghjkl1234567890qwertyuiopasdfghjklzxcvbnmkNCOUZFQjMzRTk1MTAiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJGRm1Kc0ZmSnd6aEFlQUNvdW15NV9yTS1sUkEifQ.eyJuYmYiOjE1NTgwMDU0ODksImV4cCI6MTU1ODAwOTA4OSwiaXNzIjoiaHR0cHM6Ly9hdXRoLnRydWVsYXllci5jb20iLCJhdWQiOlsiaHR0cHM6Ly9hdXRoLnRydWVsYXllci5jb20vcmVzb3VyY2VzIiwiaW5mb19hcGkiLCJhY2NvdW50c19hcGkiLCJ0cmFuc2FjdGlvbnNfYXBpIiwiYmFsYW5jZV9hcGkiLCJjYXJkc19hcGkiLCJkaXJlY3RfZGViaXRzX2FwaSIsInN0YW5kaW5nX29yZGVyc19hcGkiLCJiZW5lZmljaWFyaWVzX2FwaSIsInByb2R1Y3RzX2FwaSJdLCJjbGllbnRfaWQiOiJ1cGRyYWZ0bWFzdGVyLTUyZGI3ZCIsInN1YiI6Ikc4QUlRSWcxaUdEcnV6K2FwQjNTUTAyV1EyR0Rwb2RGR3E4RGp3QUkxT1E9IiwiYXV0aF90aW1lIjoxNTU4MDA1NDg3LCJpZHAiOiJsb2NhbCIsImNvbm5lY3Rvcl9pZCI6Im1vY2siLCJjcmVkZW50aWFsc19rZXkiOiJkNjUxMThiNTYxMDIzMzhmYWIwMjk2YzMxMzlmMGFmYTVkNmEyMjg3YjhkMDkwMWFlYzJlYzZjMzc1MGRkMzZlIiwicHJpdmFjeV9wb2xpY3kiOiJGZWIyMDE5Iiwic2NvcGUiOlsiaW5mbyIsImFjY291bnRzIiwidHJhbnNhY3Rpb25zIiwiYmFsYW5jZSIsImNhcmRzIiwiZGlyZWN0X2RlYml0cyIsInN0YW5kaW5nX29yZGVycyIsImJlbmVmaWNpYXJpZXMiLCJwcm9kdWN0cyIsIm9mZmxpbmVfYWNjZXNzIl0sImFtciI6WyJwd2QiXX0.XrLaL_fd4U9U74kPv5Cq-oEKgtKDxtj41_Q1e0jinFvt79sSaOsz8XXjOCq1Ihra95d9h9vDiFhu77HkxfNkfrC5D93Vzc0qTJka2XrHvVThA4T7BXhxkFPBhNEWvnI_HwG_Q3rbZIsAzduTKI8uFmhC45pZUbAAxIaE-v6rdvyLZhuw2VRLQQpdHNG3-z_RQT4kFdH97-CAp2G0cP9mPCnRhHUFdDGwYQf_BpPQZX1UZ75bGcQXlRQqL7IGpl27Z0jvHsKThNQZ-H-JPqgjfSQDf9Du5829pU0d55rUNQR4KAeFF4qcA8ba-Hg0oBev7Vm0lvHjodruNB90cs2eqA',
        'expires_in': 3600,
        'refresh_token': '1234567890wertyuioasdfghjklzxcvbnm1234567890qwertyuioasdfghjklf1',
        'token_type': 'Bearer'
    }


def simulate_response_token_meta_data():
    # TRUELAYER_TOKEN_META_URL
    return {
        'results': [{
            'client_id': 'updraftclient-1234id',
            'credentials_id': 'qwertyuio1234567890qwertyuioQWERTYUI12341OQ=',
            'privacy_policy': 'Feb2019',
            'provider': {
                'display_name': 'Mock',
                'logo_uri': 'https://test.com/banks-icons/mock-icon.svg',
                'provider_id': 'mock'},
            'scopes': [
                'info',
                'accounts',
                'transactions',
                'balance',
                'cards',
                'offline_access']}],
        'status': 'Succeeded'}


def simulate_response_transactions(empty=False, results_count=22):
    if empty:
        return {
            "status": "Succeeded",
            "results": []
        }
    results = [{
        "amount": -7.99,
        "currency": "GBP",
        "description": "AMAZON PRIME",
        "merchant_name": "AMAZON PRIME",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-19T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Shopping",
            "General"
        ],
        "transaction_id": "qwerty1234567890qwerty1234567890",
        "transaction_type": "DEBIT"
    }, {
        "amount": 450.0,
        "currency": "GBP",
        "description": "CLEVELEYS.COM",
        "meta": {
            "provider_transaction_category": "DEP"
        },
        "timestamp": "2019-05-19T00:00:00+00:00",
        "transaction_category": "CREDIT",
        "transaction_classification": [],
        "transaction_id": "0a660d2d542c8fef309c83f2c6c96fa5",
        "transaction_type": "CREDIT"
    }, {
        "amount": 137.6,
        "currency": "GBP",
        "description": "MR JOHN SMITH",
        "meta": {
            "provider_transaction_category": "BGC"
        },
        "timestamp": "2019-05-17T00:00:00+00:00",
        "transaction_category": "CREDIT",
        "transaction_classification": [],
        "transaction_id": "9d44296c316c435f5da3734e7d05d681",
        "transaction_type": "CREDIT"
    }, {
        "amount": -12.73,
        "currency": "GBP",
        "description": "AA INSURANCE",
        "merchant_name": "AA INSURANCE",
        "meta": {
            "provider_transaction_category": "DD"
        },
        "timestamp": "2019-05-17T00:00:00+00:00",
        "transaction_category": "DIRECT_DEBIT",
        "transaction_classification": [
            "Auto & Transport",
            "Auto Insurance"
        ],
        "transaction_id": "ddc689b8a24d14468d16e20023f0427f",
        "transaction_type": "DEBIT"
    }, {
        "amount": -20.0,
        "currency": "GBP",
        "description": "MORRISONS PETROL",
        "merchant_name": "MORRISONS PETROL STATION",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-17T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Auto & Transport",
            "Gas & Fuel"
        ],
        "transaction_id": "a3d02a62492700e80d42f08563662888",
        "transaction_type": "DEBIT"
    }, {
        "amount": -0.63,
        "currency": "GBP",
        "description": "SAVE THE CHANGE",
        "meta": {
            "provider_transaction_category": "BP"
        },
        "timestamp": "2019-05-17T00:00:00+00:00",
        "transaction_category": "BILL_PAYMENT",
        "transaction_classification": [],
        "transaction_id": "ae1849e8f19a49d457a2022a03f7aca2",
        "transaction_type": "DEBIT"
    }, {
        "amount": -6.37,
        "currency": "GBP",
        "description": "TAILSCOM",
        "merchant_name": "TAILS",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-17T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Shopping",
            "Pets"
        ],
        "transaction_id": "539ab379ece7cf66087f9e77c72c7acf",
        "transaction_type": "DEBIT"
    }, {
        "amount": -32.0,
        "currency": "GBP",
        "description": "ASDA STOES LTD",
        "merchant_name": "ASDA",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-15T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Shopping",
            "Groceries"
        ],
        "transaction_id": "9267708a8f9188cbaeccfd117b504c40",
        "transaction_type": "DEBIT"
    }, {
        "amount": -0.55,
        "currency": "GBP",
        "description": "SAVE THE CHANGE",
        "meta": {
            "provider_transaction_category": "BP"
        },
        "timestamp": "2019-05-15T00:00:00+00:00",
        "transaction_category": "BILL_PAYMENT",
        "transaction_classification": [],
        "transaction_id": "c7a2510ba4876ef47f76c4d30fb17f2a",
        "transaction_type": "DEBIT"
    }, {
        "amount": -15.45,
        "currency": "GBP",
        "description": "AMAZON BLACK FRIDAY",
        "merchant_name": "AMAZON",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-15T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Shopping",
            "General"
        ],
        "transaction_id": "f72cd88c3e6854c4aa73daccf9b6c0c8",
        "transaction_type": "DEBIT"
    }, {
        "amount": -0.5,
        "currency": "GBP",
        "description": "SAVE THE CHANGE",
        "meta": {
            "provider_transaction_category": "BP"
        },
        "timestamp": "2019-05-15T00:00:00+00:00",
        "transaction_category": "BILL_PAYMENT",
        "transaction_classification": [],
        "transaction_id": "c110d8d4e47bdc9c5e3b14321d80af25",
        "transaction_type": "DEBIT"
    }, {
        "amount": -3.59,
        "currency": "GBP",
        "description": "TESCO EXTRA",
        "merchant_name": "TESCO",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-15T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Shopping",
            "Groceries"
        ],
        "transaction_id": "009cd91c4cc6388b8e595fa13041a336",
        "transaction_type": "DEBIT"
    }, {
        "amount": -10.0,
        "currency": "GBP",
        "description": "PAYPAL EBAY",
        "merchant_name": "EBAY",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-15T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Shopping",
            "General"
        ],
        "transaction_id": "55124c408555bfc4543e76a4ad54d5c1",
        "transaction_type": "DEBIT"
    }, {
        "amount": -2.0,
        "currency": "GBP",
        "description": "PAYPAL BETFRED",
        "merchant_name": "BETFRED",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-14T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Gambling",
            "Betting"
        ],
        "transaction_id": "28892dd555e8f63c1c3df45a9b48a33a",
        "transaction_type": "DEBIT"
    }, {
        "amount": -20.5,
        "currency": "GBP",
        "description": "MT SECURETRADE LIM",
        "merchant_name": "",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-14T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Gambling",
            "Casino"
        ],
        "transaction_id": "0fbfa22f590a7ebd3e993de270e3f14b",
        "transaction_type": "DEBIT"
    }, {
        "amount": 43.5,
        "currency": "GBP",
        "description": "MT SECURETRADE LIM",
        "merchant_name": "",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-14T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Gambling",
            "Casino"
        ],
        "transaction_id": "b4424511883b9292661b9eec0b4a59a9",
        "transaction_type": "CREDIT"
    }, {
        "amount": -0.41,
        "currency": "GBP",
        "description": "SAVE THE CHANGE",
        "meta": {
            "provider_transaction_category": "BP"
        },
        "timestamp": "2019-05-12T00:00:00+00:00",
        "transaction_category": "BILL_PAYMENT",
        "transaction_classification": [],
        "transaction_id": "d354022fcdaf2e8fef69bd28d2e9986e",
        "transaction_type": "DEBIT"
    }, {
        "amount": -23.11,
        "currency": "GBP",
        "description": "AA MEMBERSHIP",
        "merchant_name": "AA MEMBERSHIP",
        "meta": {
            "provider_transaction_category": "DD"
        },
        "timestamp": "2019-05-12T00:00:00+00:00",
        "transaction_category": "DIRECT_DEBIT",
        "transaction_classification": [
            "Auto & Transport",
            "Service & Auto Parts"
        ],
        "transaction_id": "aaeb2ea04b6cd7f5fc2071c6b6a1ea2c",
        "transaction_type": "DEBIT"
    }, {
        "amount": 50.0,
        "currency": "GBP",
        "description": "CIRCLE UK TRADING",
        "meta": {
            "provider_transaction_category": "FPI"
        },
        "timestamp": "2019-05-11T00:00:00+00:00",
        "transaction_category": "CREDIT",
        "transaction_classification": [],
        "transaction_id": "8056499a03fbf69ff728a3a4e17c649d",
        "transaction_type": "CREDIT"
    }, {
        "amount": 50.0,
        "currency": "GBP",
        "description": "MS JANE DOE",
        "meta": {
            "provider_transaction_category": "TFR"
        },
        "timestamp": "2019-05-11T00:00:00+00:00",
        "transaction_category": "TRANSFER",
        "transaction_classification": [],
        "transaction_id": "0d17d571d10348f559586bdb60fa874b",
        "transaction_type": "CREDIT"
    }, {
        "amount": 22.42,
        "currency": "GBP",
        "description": "WORKING TAX CREDIT",
        "meta": {
            "provider_transaction_category": "BGC"
        },
        "timestamp": "2019-05-11T00:00:00+00:00",
        "transaction_category": "CREDIT",
        "transaction_classification": [],
        "transaction_id": "46d9bc70efcb0283a3508a3037487ff9",
        "transaction_type": "CREDIT"
    }, {
        "amount": -17.0,
        "currency": "GBP",
        "description": "BUTLINS HOLIDAYS",
        "merchant_name": "BUTLINS",
        "meta": {
            "provider_transaction_category": "DEB"
        },
        "timestamp": "2019-05-11T00:00:00+00:00",
        "transaction_category": "PURCHASE",
        "transaction_classification": [
            "Travel",
            "Vacation"
        ],
        "transaction_id": "b4dcda367d12157c5e527be1cce172d6",
        "transaction_type": "DEBIT"
    }]
    response = {
        "status": "Succeeded",
        "results": results[:results_count],
    }
    return response
