import responses
import urllib
from freezegun import freeze_time
from model_mommy import mommy
from django.test import TestCase
from django.conf import settings

from truelayer.tests.example_responses import simulate_response_transactions
from truelayer.models import BankAccount, AsyncRequest
from truelayer.tasks import request_async_transactions
from truelayer.utils import get_webhook_uri


class TestRequestAsyncTransactionsFromTrueLayer(TestCase):
    ''' Check the from - to period requested from True Layer'''
    def setUp(self):
        pass
        self.token = mommy.make_recipe('truelayer.token')
        self.profile = mommy.make_recipe('truelayer.profile', token=self.token)
        self.provider = self.profile.provider
        self.account = mommy.make(BankAccount, bank_profile=self.profile, provider=self.provider)
        self.example_task_id = 'abc123ab-4567-8901-a89b-12bcde4b40ee'
        self.example_results_uri = settings.TRUELAYER_API_URL + '/results/' + self.example_task_id
        self.example_response = {'results_uri': self.example_results_uri,
                                 'status': 'Queued',
                                 'task_id': self.example_task_id}
        self.transactions_url = settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=self.account.account_id)

    @responses.activate
    def test_request_month(self):
        ''' Verify when request_async_transactions called with a month period,
        the True Layer api is requested with from = the day of previous month
        '''
        params = {
            'from': '2019-04-01T09:00:00+00:00',
            'to': '2019-05-10T09:00:00+00:00',
            'async': 'true',
            'webhook_uri': get_webhook_uri()
        }
        query = urllib.parse.urlencode(params, quote_via=urllib.parse.quote)
        expected_url_to_hit = self.transactions_url + '?' + query

        responses.add(responses.GET, expected_url_to_hit,
                      json=self.example_response, status=200, match_querystring=True)

        with freeze_time("2019-05-10 09:00"):
            request_async_transactions(self.account.account_id, self.token.id, period='month')
        assert AsyncRequest.objects.count() == 1

    @responses.activate
    def test_request_year(self):
        ''' Verify when request_async_transactions called with an year period,
        the True Layer api is requested with param from = 354 days back in time
        '''
        params = {
            'from': '2018-05-11T09:00:00+00:00',
            'to': '2019-05-10T09:00:00+00:00',
            'async': 'true',
            'webhook_uri': get_webhook_uri()
        }
        query = urllib.parse.urlencode(params, quote_via=urllib.parse.quote)
        expected_url_to_hit = self.transactions_url + '?' + query

        responses.add(responses.GET, expected_url_to_hit,
                      json=self.example_response, status=200, match_querystring=True)

        with freeze_time("2019-05-10 09:00"):
            request_async_transactions(self.account.account_id, self.token.id, period='year')
        assert AsyncRequest.objects.count() == 1
