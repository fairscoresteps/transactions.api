import pytest
import responses
from django.conf import settings
from django.test import TestCase
from model_mommy import mommy

from truelayer.exceptions import ResponseValidationError
from truelayer.models import Provider, BankProfile, BankAccount, Token
from truelayer.tests.example_responses import (simulate_response_exchange_code_with_access_token,
                                               simulate_response_token_meta_data)
from truelayer.views import obtain_access_token


class TestRequestAccounts(TestCase):
    def setUp(self):
        self.user = mommy.make(settings.AUTH_USER_MODEL)
        self.code = 'qwerty123456789qwerty'
        pass

    @responses.activate
    def test_obtain_access_token_when_connect_new_profile(self):
        responses.add(responses.POST, settings.TRUELAYER_TOKEN_URL,
                      json=simulate_response_exchange_code_with_access_token(), status=200)
        responses.add(responses.GET, settings.TRUELAYER_TOKEN_META_URL,
                      json=simulate_response_token_meta_data(), status=200)
        assert Provider.objects.count() == 0
        assert BankProfile.objects.count() == 0
        assert self.user.bank_profiles.count() == 0

        # new token obtained (state is user id, not user profile)
        obtain_access_token(code=self.code, state=f'user_{self.user.id}')
        assert Provider.objects.count() == 1
        assert self.user.bank_profiles.count() == 1
        profile = self.user.bank_profiles.all()[0]
        assert profile.token is not None
        assert profile.token.raw_response is not None
        assert 'results' in profile.token.raw_response
        assert profile.provider.provider_id == 'mock'

    @responses.activate
    def test_obtain_access_token_when_reconnect_existing_profile(self):
        existing_profile = mommy.make_recipe('truelayer.profile', user=self.user)
        existing_account = mommy.make(BankAccount, bank_profile=existing_profile,
                                      provider=existing_profile.provider)
        old_token_id = existing_profile.token.id
        assert existing_profile.provider.provider_id == "bank_1"
        assert self.user.bank_profiles.count() == 1
        assert BankAccount.objects.count() == 1
        assert BankProfile.objects.count() == 1
        assert Provider.objects.count() == 1
        assert Token.objects.count() == 1

        responses.add(responses.POST, settings.TRUELAYER_TOKEN_URL,
                      json=simulate_response_exchange_code_with_access_token(), status=200)
        responses.add(responses.GET, settings.TRUELAYER_TOKEN_META_URL,
                      json=simulate_response_token_meta_data(), status=200)

        # new token obtained (state is user id, not user profile)
        obtain_access_token(code=self.code, state=f'profile_{existing_profile.id}')
        profile = self.user.bank_profiles.all()[0]
        assert profile.token is not None
        assert profile.token.id != old_token_id
        assert profile.token.raw_response is not None
        assert 'results' in profile.token.raw_response
        assert Token.objects.count() == 1  # old token deleted
        assert profile.provider.provider_id == 'mock'
        assert Provider.objects.count() == 2  # cause user connected different provider - Mock
        assert BankAccount.objects.count() == 0  # all old accounts deleted
        assert self.user.bank_profiles.count() == 1

    @responses.activate
    def test_request_for_exchange_failed(self):
        responses.add(responses.POST, settings.TRUELAYER_TOKEN_URL,
                      json={}, status=400)
        responses.add(responses.GET, settings.TRUELAYER_TOKEN_META_URL,
                      json=simulate_response_token_meta_data(), status=200)
        with pytest.raises(ResponseValidationError):
            assert obtain_access_token(code=self.code, state=f'user_1')

        with pytest.raises(ResponseValidationError):
            assert obtain_access_token(code=self.code, state=f'profile_1')

    @responses.activate
    def test_request_for_exchange_invalid_data(self):
        responses.add(responses.POST, settings.TRUELAYER_TOKEN_URL,
                      json={'access_token': ''}, status=200)
        responses.add(responses.GET, settings.TRUELAYER_TOKEN_META_URL,
                      json=simulate_response_token_meta_data(), status=200)
        with pytest.raises(ResponseValidationError):
            assert obtain_access_token(code=self.code, state=f'user_1')
        with pytest.raises(ResponseValidationError):
            assert obtain_access_token(code=self.code, state=f'profile_1')

    @responses.activate
    def test_request_for_meta_data_failed(self):
        responses.add(responses.POST, settings.TRUELAYER_TOKEN_URL,
                      json=simulate_response_exchange_code_with_access_token(), status=200)
        responses.add(responses.GET, settings.TRUELAYER_TOKEN_META_URL,
                      json={'status': 'Failed'}, status=400)
        with pytest.raises(ResponseValidationError):
            assert obtain_access_token(code=self.code, state=f'user_1')
        with pytest.raises(ResponseValidationError):
            assert obtain_access_token(code=self.code, state=f'profile_1')

    @responses.activate
    def test_request_for_meta_data_missing_provider_raises_error(self):
        # we do not expect True layer to change the response and to skip provider
        # but if that happen, it'll require immidiate reaction on our end
        responses.add(responses.POST, settings.TRUELAYER_TOKEN_URL,
                      json=simulate_response_exchange_code_with_access_token(), status=200)
        responses.add(responses.GET, settings.TRUELAYER_TOKEN_META_URL,
                      json={}, status=200)
        with pytest.raises(ResponseValidationError):
            assert obtain_access_token(code=self.code, state=f'user_1')
        with pytest.raises(ResponseValidationError):
            assert obtain_access_token(code=self.code, state=f'profile_1')
