# Basic test to check the DB is working
from django.test import TestCase
from rest_framework.test import APITestCase
from model_mommy import mommy
from truelayer.models import BankAccount, AccountBalance, CardBalance, BankProfile, CreditCard
from authentication.models import CustomUser
from django.urls import reverse


class BasicTest(TestCase):
    def test_create_account(self):
        account = mommy.make(BankAccount)
        mommy.make(AccountBalance, bank_account=account)
        db_acct = BankAccount.objects.get(id=account.id)
        assert account.id == db_acct.id
        assert account.balance.current == db_acct.balance.current


class AuthenticatedTestCase(APITestCase):
    def setUp(self):
        super().setUp()
        self.user = mommy.make(CustomUser, is_active=True)
        self.client.force_authenticate(user=self.user)


class UpdateAccountTest(APITestCase):
    def setUp(self):
        super().setUp()
        self.user = mommy.make(CustomUser, is_active=True)
        self.account_1 = mommy.make(BankAccount, bank_profile=mommy.make(BankProfile, user=self.user))
        self.account_2 = mommy.make(BankAccount, bank_profile=mommy.make(BankProfile, user=self.user))
        self.url = reverse('update_account', kwargs={'pk': self.account_1.pk})
        self.list_url = reverse('user_accounts')
        self.client.force_authenticate(user=self.user)

    def test_list_of_accounts(self):
        r = self.client.get(self.list_url)
        assert r.status_code == 200
        assert len(r.json()) == 2
        assert 'balance' in r.json()[0].keys()

    def test_update_account_just_status(self):
        r = self.client.put(self.url, {'status': 'USER_HIDDEN'}, format='json')
        assert r.status_code == 200
        assert r.json()['status'] == 'USER_HIDDEN'
        r = self.client.get(self.list_url)
        assert r.status_code == 200
        assert len(r.json()) == 1

    def test_update_account_all_data(self):
        r = self.client.get(self.url)
        assert r.status_code == 200
        new_acc = r.json()
        new_acc['status'] = 'USER_HIDDEN'
        r = self.client.put(self.url, new_acc, format='json')
        assert r.status_code == 200
        assert r.json()['status'] == 'USER_HIDDEN'
        r = self.client.get(self.list_url)
        assert r.status_code == 200
        assert len(r.json()) == 1


class UpdateCreditCardTest(APITestCase):
    def setUp(self):
        super().setUp()
        self.user = mommy.make(CustomUser, is_active=True)
        self.card_1 = mommy.make(CreditCard, bank_profile=mommy.make(BankProfile, user=self.user))
        self.card_2 = mommy.make(CreditCard, bank_profile=mommy.make(BankProfile, user=self.user))
        mommy.make(CardBalance, credit_card=self.card_1)
        mommy.make(CardBalance, credit_card=self.card_2)
        self.url = reverse('update_card', kwargs={'pk': self.card_1.pk})
        self.list_url = reverse('user_cards')
        self.client.force_authenticate(user=self.user)

    def test_list_of_accounts(self):
        r = self.client.get(self.list_url)
        assert r.status_code == 200
        assert len(r.json()) == 2

    def test_update_account_just_status(self):
        r = self.client.put(self.url, {'status': 'USER_HIDDEN'}, format='json')
        assert r.status_code == 200
        assert r.json()['status'] == 'USER_HIDDEN'
        r = self.client.get(self.list_url)
        assert r.status_code == 200
        assert len(r.json()) == 1

    def test_update_account_all_data(self):
        r = self.client.get(self.url)
        assert r.status_code == 200
        new_card = r.json()
        new_card['status'] = 'USER_HIDDEN'
        r = self.client.put(self.url, new_card, format='json')
        assert r.status_code == 200
        assert r.json()['status'] == 'USER_HIDDEN'
        r = self.client.get(self.list_url)
        assert r.status_code == 200
        assert len(r.json()) == 1
