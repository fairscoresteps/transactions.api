import mock
import pytest
import responses
from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy
from rest_framework.test import APITestCase

from truelayer.models import AsyncRequest, BankAccount
from truelayer.tests.example_responses import simulate_response_for_accounts
from truelayer.utils import get_webhook_uri
from truelayer.requests import send_async_request_to_true_layer


class TestAsyncRequestsToTrueLayer(TestCase):
    def setUp(self):
        self.token = mommy.make_recipe('truelayer.token')  # active token, that expires in the next 3600s
        self.profile = mommy.make_recipe('truelayer.profile', token=self.token)
        self.account = mommy.make(BankAccount, bank_profile=self.profile)
        self.example_task_id = 'abc123ab-4567-8901-a89b-12bcde4b40ee'

    @responses.activate
    def test_track_async_request_for_transactions(self):
        ''' when sending async request, AsyncRequest record is kept for reference'''
        example_results_uri = settings.TRUELAYER_API_URL + '/results/' + self.example_task_id
        example_response = {'results_uri': example_results_uri,
                            'status': 'Queued',
                            'task_id': self.example_task_id}
        transactions_url = settings.TRUELAYER_TRANSACTIONS_URL.format(account_id=self.account.account_id)
        expected_url_to_hit = transactions_url + '?async=true&webhook_uri=' + get_webhook_uri()

        responses.add(responses.GET, expected_url_to_hit, json=example_response, status=200)

        send_async_request_to_true_layer(url=transactions_url,
                                         token_id=self.account.bank_profile.token.id,
                                         request_type=AsyncRequest.TYPE_TRANSACTIONS,
                                         extra_data={'account_id': self.account.account_id})
        assert AsyncRequest.objects.count() == 1
        db_record = AsyncRequest.objects.all()[0]
        assert db_record.request_type == AsyncRequest.TYPE_TRANSACTIONS
        assert db_record.status == AsyncRequest.STATUS_QUEUED
        assert db_record.token == self.token
        assert db_record.task_id == self.example_task_id
        assert db_record.results_uri == example_results_uri
        assert db_record.extra_data['account_id'] == self.account.account_id

    @responses.activate
    def test_track_async_request_for_accounts(self):
        ''' when sending async request, AsyncRequest record is kept for reference'''
        example_results_uri = settings.TRUELAYER_API_URL + '/results/' + self.example_task_id
        example_response = {'results_uri': example_results_uri,
                            'status': 'Queued',
                            'task_id': self.example_task_id}
        expected_url_to_hit = settings.TRUELAYER_ACCOUNTS_URL + '?async=true&webhook_uri=' + get_webhook_uri()

        responses.add(responses.GET, expected_url_to_hit, json=example_response, status=200)

        send_async_request_to_true_layer(url=settings.TRUELAYER_ACCOUNTS_URL,
                                         token_id=self.account.bank_profile.token.id,
                                         request_type=AsyncRequest.TYPE_ACCOUNTS,
                                         extra_data={})
        assert AsyncRequest.objects.count() == 1
        db_record = AsyncRequest.objects.all()[0]
        assert db_record.request_type == AsyncRequest.TYPE_ACCOUNTS
        assert db_record.status == AsyncRequest.STATUS_QUEUED
        assert db_record.token == self.token
        assert db_record.task_id == self.example_task_id
        assert db_record.results_uri == example_results_uri
        assert db_record.extra_data == {}


@mock.patch('truelayer.views.logger')
class TestWebhookURI(APITestCase):
    '''Simulating how True Layer notify us when data is ready for us to take'''
    def setUp(self):
        self.token = mommy.make_recipe('truelayer.token')
        self.task_id = '12fa5aea-e7e2-48ca-93eb-0abf75805cef'
        self.example_results_uri = settings.TRUELAYER_API_URL + '/results/' + self.task_id
        self.credentials_id = 'z5f7u7CxoVxjtBmFUgz8MZPpz8iIH5t406bL1qBonK0='
        self.webhook_uri = reverse('true_layer_webhook_uri')
        self.post_data = {
            'credentials_id': self.credentials_id,
            'request_timestamp': '2019-05-20T11:07:16.8631281+00:00',
            'request_uri': settings.TRUELAYER_ACCOUNTS_URL,
            'results_uri': self.example_results_uri,
            'status': 'Succeeded',
            'task_id': self.task_id}

    def test_truelayer_notifies_on_accounts_ready(self, mocked_logger):
        async_record = mommy.make_recipe('truelayer.async_record_accounts',
                                         token=self.token,
                                         task_id=self.task_id,
                                         results_uri=self.example_results_uri)
        # when webhook url gets hit, the api will make request for accounts
        responses.add(responses.GET, settings.TRUELAYER_ACCOUNTS_URL,
                      json=simulate_response_for_accounts(), status=200)

        r = self.client.post(self.webhook_uri, self.post_data)
        assert r.status_code == 200
        async_record_db = AsyncRequest.objects.get(id=async_record.id)
        assert async_record_db.status == AsyncRequest.STATUS_SUCCEEDED
        assert async_record_db.credentials_id == self.credentials_id
        assert async_record_db.task_id == self.task_id
        assert async_record_db.request_type == AsyncRequest.TYPE_ACCOUNTS

    def test_truelayer_post_weird_data_to_webhook_uri(self, mocked_logger):
        async_record = mommy.make_recipe('truelayer.async_record_accounts',
                                         token=self.token,
                                         task_id=self.task_id,
                                         results_uri=self.example_results_uri)

        with pytest.raises(Exception):
            self.client.post(self.webhook_uri, {'data': 'some data'})

        async_record_db = AsyncRequest.objects.get(id=async_record.id)
        assert async_record_db.status == AsyncRequest.STATUS_QUEUED

    def test_truelayer_post_unknown_task_id(self, mocked_logger):
        async_record = mommy.make_recipe('truelayer.async_record_accounts',
                                         token=self.token,
                                         task_id=self.task_id,
                                         results_uri=self.example_results_uri)
        self.post_data['task_id'] = 'unknown1234'
        self.client.post(self.webhook_uri, self.post_data)
        mocked_logger.error.assert_called_once()
        async_record_db = AsyncRequest.objects.get(id=async_record.id)
        assert async_record_db.status == AsyncRequest.STATUS_QUEUED

    def test_truelayer_post_status_failed(self, mocked_logger):
        async_record = mommy.make_recipe('truelayer.async_record_accounts',
                                         token=self.token,
                                         task_id=self.task_id,
                                         credentials_id=self.credentials_id,
                                         results_uri=self.example_results_uri)
        self.post_data['status'] = AsyncRequest.STATUS_FAILED
        self.client.post(self.webhook_uri, self.post_data)
        mocked_logger.error.assert_called_once()
        async_record_db = AsyncRequest.objects.get(id=async_record.id)
        assert async_record_db.status == AsyncRequest.STATUS_FAILED
        assert async_record_db.credentials_id == async_record.credentials_id
        assert async_record_db.task_id == self.task_id
        assert async_record_db.request_type == async_record.request_type
