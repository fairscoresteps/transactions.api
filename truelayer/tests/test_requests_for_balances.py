import mock
import responses

from django.test import TestCase
from django.conf import settings
from model_mommy import mommy

from truelayer.models import BankAccount, AccountBalance, CardBalance, CreditCard
from truelayer.tasks import request_balance, request_balance_for_card
from truelayer.tests.example_responses import (simulate_response_for_balance,
                                               simulate_response_for_card_balance)


@mock.patch('truelayer.tasks.logger')
class TestRequestAccountBalanceFromTrueLayer(TestCase):
    def setUp(self):
        self.token = mommy.make_recipe('truelayer.token')
        self.profile = mommy.make_recipe('truelayer.profile', token=self.token)

    @responses.activate
    def test_request_balance(self, mocked_logger):
        account = mommy.make(BankAccount, bank_profile=self.profile)
        assert AccountBalance.objects.count() == 0
        responses.add(responses.GET,
                      settings.TRUELAYER_BALANCE_URL.format(account_id=account.account_id),
                      json=simulate_response_for_balance(), status=200)

        request_balance(self.token, account)
        assert AccountBalance.objects.count() == 1
        assert account.balance.available == 585
        assert account.balance.currency == "GBP"
        assert account.balance.current == 85
        assert account.balance.overdraft == 500

    @responses.activate
    def test_request_same_balance_updates_balance_record(self, mocked_logger):
        account = mommy.make(BankAccount, bank_profile=self.profile)
        mommy.make(AccountBalance,
                   bank_account=account,
                   update_timestamp="2019-05-16T10:30:00.0927089Z",
                   available=585, currency="GBP", current=85, overdraft=500)
        assert AccountBalance.objects.count() == 1
        responses.add(responses.GET,
                      settings.TRUELAYER_BALANCE_URL.format(account_id=account.account_id),
                      json=simulate_response_for_balance(), status=200)

        request_balance(self.token, account)
        assert AccountBalance.objects.count() == 1
        assert account.balance.available == 585
        assert account.balance.currency == "GBP"
        assert account.balance.current == 85
        assert account.balance.overdraft == 500

    @responses.activate
    def test_request_no_balance_no_updates_on_existing_record(self, mocked_logger):
        account = mommy.make(BankAccount, bank_profile=self.profile)
        mommy.make_recipe('truelayer.balance', bank_account=account)
        assert AccountBalance.objects.count() == 1
        responses.add(responses.GET,
                      settings.TRUELAYER_BALANCE_URL.format(account_id=account.account_id),
                      json=simulate_response_for_balance(empty=True), status=200)

        request_balance(self.token, account)
        assert AccountBalance.objects.count() == 1
        assert account.balance.available == 125
        assert account.balance.currency == "GBP"
        assert account.balance.current == 25
        assert account.balance.overdraft == 100

    @responses.activate
    def test_request_no_balance(self, mocked_logger):
        account = mommy.make(BankAccount, bank_profile=self.profile)
        responses.add(responses.GET,
                      settings.TRUELAYER_BALANCE_URL.format(account_id=account.account_id),
                      json=simulate_response_for_balance(empty=True), status=200)

        request_balance(self.token, account)
        assert account.balance is None
        assert AccountBalance.objects.count() == 0

    @responses.activate
    def test_response_status_not_200_no_effect_on_existing(self, mocked_logger):
        account = mommy.make(BankAccount, bank_profile=self.profile)
        mommy.make_recipe('truelayer.balance', bank_account=account)
        responses.add(responses.GET,
                      settings.TRUELAYER_BALANCE_URL.format(account_id=account.account_id),
                      json={}, status=400)
        request_balance(self.token, account)
        assert account.balance is not None
        assert account.balance.available == 125

    @responses.activate
    def test_missing_results_key_in_response(self, mocked_logger):
        account = mommy.make(BankAccount, bank_profile=self.profile)
        mommy.make_recipe('truelayer.balance', bank_account=account)
        responses.add(responses.GET,
                      settings.TRUELAYER_BALANCE_URL.format(account_id=account.account_id),
                      json={}, status=200)

        request_balance(self.token, account)
        mocked_logger.error.assert_called_once()
        assert account.balance is not None
        assert account.balance.available == 125


@mock.patch('truelayer.tasks.logger')
class TestRequestCardBalanceFromTrueLayer(TestCase):
    def setUp(self):
        self.token = mommy.make_recipe('truelayer.token')
        self.profile = mommy.make_recipe('truelayer.profile', token=self.token)

    @responses.activate
    def test_request_balance_for_card(self, mocked_logger):
        card = mommy.make(CreditCard, bank_profile=self.profile)
        assert CardBalance.objects.count() == 0
        responses.add(responses.GET,
                      settings.TRUELAYER_CARD_BALANCE_URL.format(account_id=card.account_id),
                      json=simulate_response_for_card_balance(), status=200)

        request_balance_for_card(self.token, card)
        assert CardBalance.objects.count() == 1
        assert card.balance.available == 80
        assert card.balance.currency == "GBP"
        assert card.balance.current == 40
        assert card.balance.credit_limit == 120

    @responses.activate
    def test_request_same_balance_updates_balance_record(self, mocked_logger):
        card = mommy.make(CreditCard, bank_profile=self.profile)
        mommy.make(CardBalance,
                   credit_card=card,
                   update_timestamp="2019-05-16T10:30:00.0927089Z",
                   available=85, currency="GBP", current=45, credit_limit=130)
        assert CardBalance.objects.count() == 1
        responses.add(responses.GET,
                      settings.TRUELAYER_CARD_BALANCE_URL.format(account_id=card.account_id),
                      json=simulate_response_for_card_balance(), status=200)

        request_balance_for_card(self.token, card)
        assert CardBalance.objects.count() == 1
        card_db = CreditCard.objects.get(id=card.id)
        assert card_db.balance.available == 80
        assert card_db.balance.currency == "GBP"
        assert card_db.balance.current == 40
        assert card_db.balance.credit_limit == 120

    @responses.activate
    def test_request_no_balance_no_updates_on_existing_record(self, mocked_logger):
        card = mommy.make(CreditCard, bank_profile=self.profile)
        mommy.make_recipe('truelayer.card_balance', credit_card=card)
        assert CardBalance.objects.count() == 1
        responses.add(responses.GET,
                      settings.TRUELAYER_CARD_BALANCE_URL.format(account_id=card.account_id),
                      json=simulate_response_for_card_balance(empty=True), status=200)

        request_balance_for_card(self.token, card)
        assert CardBalance.objects.count() == 1
        assert card.balance.available == 80
        assert card.balance.currency == "GBP"
        assert card.balance.current == 40
        assert card.balance.credit_limit == 120

    @responses.activate
    def test_request_no_balance(self, mocked_logger):
        card = mommy.make(CreditCard, bank_profile=self.profile)
        responses.add(responses.GET,
                      settings.TRUELAYER_CARD_BALANCE_URL.format(account_id=card.account_id),
                      json=simulate_response_for_card_balance(empty=True), status=200)

        request_balance_for_card(self.token, card)
        assert card.balance is None
        assert CardBalance.objects.count() == 0

    @responses.activate
    def test_response_status_not_200_no_effect_on_existing(self, mocked_logger):
        card = mommy.make(CreditCard, bank_profile=self.profile)
        mommy.make_recipe('truelayer.card_balance', credit_card=card)
        responses.add(responses.GET,
                      settings.TRUELAYER_CARD_BALANCE_URL.format(account_id=card.account_id),
                      json={}, status=400)
        request_balance_for_card(self.token, card)
        assert card.balance is not None
        assert card.balance.available == 80

    @responses.activate
    def test_missing_results_key_in_response(self, mocked_logger):
        card = mommy.make(CreditCard, bank_profile=self.profile)
        mommy.make_recipe('truelayer.card_balance', credit_card=card)
        responses.add(responses.GET,
                      settings.TRUELAYER_CARD_BALANCE_URL.format(account_id=card.account_id),
                      json={}, status=200)

        request_balance_for_card(self.token, card)
        mocked_logger.error.assert_called_once()
        assert card.balance is not None
        assert card.balance.available == 80
